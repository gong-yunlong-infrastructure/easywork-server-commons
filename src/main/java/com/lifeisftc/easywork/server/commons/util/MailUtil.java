package com.lifeisftc.easywork.server.commons.util;

import com.lifeisftc.easywork.server.commons.json.JsonMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.List;

/**
 * @author gongyunlong
 */
@Component
public class MailUtil {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendDidaEmail(String[] receiver, String subject, String content) throws Exception {
        String sendName = LoadPropertyUtil.getProperty("spring.mail.username");
        sendEmail(sendName, receiver, null, subject, content);
    }


    public void sendEmail(String deliver, String[] receiver, String[] carbonCopy, String subject, String content) throws Exception {
        SimpleMailMessage message = new SimpleMailMessage();
        try {
            message.setFrom(deliver);
            message.setTo(receiver);
            if (CheckObjUtil.isNotEmpty(carbonCopy)) {
                message.setCc(carbonCopy);
            }
            message.setSubject(subject);
            message.setText(content);
            javaMailSender.send(message);
        } catch (MailException e) {
            logger.error("Send mail failed, error, message:{}", JsonMapper.toJson(message), e);
            throw new Exception(e.getMessage());
        }
    }


    public void sendEmail(String[] receiver, String subject, String content, File file) throws MessagingException {
        String deliver = LoadPropertyUtil.getProperty("spring.mail.username");
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,true);
        helper.setFrom(deliver);
        helper.setTo(receiver);
        helper.setSubject(subject);
        helper.addAttachment(file.getName(), file);
        helper.setText(content, true);
        javaMailSender.send(message);
    }
    public void sendEmailBatch(String[] receiver, String subject, String content, List<File> fileList) throws MessagingException {
        String deliver = LoadPropertyUtil.getProperty("spring.mail.username");
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message,true);
        helper.setFrom(deliver);
        helper.setTo(receiver);
        helper.setSubject(subject);
        for (File file:fileList) {
            helper.addAttachment(file.getName(), file);
        }
        helper.setText(content, true);
        javaMailSender.send(message);
    }

}
