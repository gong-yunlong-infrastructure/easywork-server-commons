package com.lifeisftc.easywork.server.commons.datasource;

import org.mybatis.spring.transaction.SpringManagedTransaction;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * 动态数据源事务管理类
 * @author gongyunlong
 */
public class CustomSpringManagedTransaction extends SpringManagedTransaction {

    public CustomSpringManagedTransaction(DataSource dynamicDataSource) {
        super(dynamicDataSource);
    }

    @Override
    public Connection getConnection() throws SQLException {
        // 判断当前线程的事务同步是否处于活动状态
        if (!TransactionSynchronizationManager.isSynchronizationActive()){
            throw new SQLException("Please add @Transactional to keep transaction can rollback!");
        }
        return super.getConnection();
    }


}

