
package com.lifeisftc.easywork.server.commons.util.auth;

import com.alibaba.fastjson.JSON;
import com.auth0.jwk.Jwk;
import com.lifeisftc.easywork.server.commons.util.LoadPropertyUtil;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.security.PublicKey;
import java.util.Base64;
import java.util.List;
import java.util.Map;

/**
 * 苹果登录验证工具
 * implementation group: 'io.jsonwebtoken', name: 'jjwt', version: '0.9.1'
 * implementation group: 'com.auth0', name: 'jwks-rsa', version: '0.9.0'
 *
 * @author gongyunlong
 */
public class AppleIdUtil {

    private final static Logger logger = LoggerFactory.getLogger(AppleIdUtil.class);

    private static final String APPLEID_PUBLICKEY_URL = "https://appleid.apple.com/auth/keys";

    private static final String ISSUER = "https://appleid.apple.com";

    /**
     *
     */
    public static final String CLIENTID = "待定";

    private final static RestTemplate REST_TEMPLATE;

    static {
        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
        int readTimeout = LoadPropertyUtil.getProperty("appleid.request.readtimeout", Integer.class, 10000);
        int connectionTimeout = LoadPropertyUtil.getProperty("appleid.request.connectiontimeout", Integer.class, 10000);
        requestFactory.setConnectTimeout(connectionTimeout);
        requestFactory.setReadTimeout(readTimeout);
        REST_TEMPLATE = new RestTemplate(requestFactory);
    }


    /**
     * 验证苹果identityToken
     *
     * @param identityToken jwt格式
     * @param audience      client_id(bundle_id)
     * @param subject       apple user_id
     * @return boolean
     */
    public static boolean verifyIdentityToken(String identityToken, String audience, String subject) {
        boolean result = false;
        CusJws cusJws = getJws(identityToken);
        if (cusJws == null) {
            logger.info("verifyIdentityToken but decode to jwt return null, identityToken:{}", identityToken);
            return false;
        }
        PublicKey publicKey = getAppleIdPublicKey(cusJws.getJwsHeader().getKid());
        if (publicKey == null) {
            logger.info("verifyIdentityToken but getAppleIdPublicKey return null, cusJws:{}", cusJws);
            return false;
        }
        JwtParser jwtParser = Jwts.parser().setSigningKey(publicKey);
        jwtParser.requireIssuer(ISSUER);
        jwtParser.requireAudience(audience);
        jwtParser.requireSubject(subject);
        try {
            Jws<Claims> claim = jwtParser.parseClaimsJws(identityToken);
            if (claim != null && claim.getBody().containsKey("auth_time")) {
                result = true;
            } else {
                logger.info("verifyIdentityToken failed by parseClaimsJws, identityToken:{}, claim:{}", identityToken, JSON.toJSONString(claim));
            }
        } catch (ExpiredJwtException e) {
            logger.info("verifyIdentityToken but token is expired, subject：{}， audience：{}， cusJws:{}", subject, audience, cusJws);
        } catch (Exception e) {
            logger.error("verifyIdentityToken failed by {}, subject：{}， audience：{}， cusJws:{}", e.getMessage(), subject, audience, cusJws);
        }
        return result;
    }

    /**
     * publicKey 放redis缓存
     * TODO 缓存待实现
     * @param kid String
     * @return PublicKey
     */
    private static PublicKey getAppleIdPublicKey(String kid) {
//        String publicKeyStr = AppleIdDao.getPublicKey();
        String publicKeyStr = null;
        PublicKey publicKey = null;
        if (publicKeyStr == null) {
            publicKeyStr = getAppleIdPublicKeyFromRemote();
            if (publicKeyStr == null) {
                return publicKey;
            }
//            AppleIdDao.setPublicKey(publicKeyStr);
        }
        try {
            publicKey = publicKeyAdapter(publicKeyStr, kid);
        } catch (Exception e) {
            logger.error("getAppleIdPublicKey failed, kid:{}", kid, e);
            return publicKey;
        }
        return publicKey;
    }

    /**
     * 将appleServer返回的publicKey转换成PublicKey对象
     *
     * @param publicKeyStr String
     * @param kid String
     * @return PublicKey
     */
    private static PublicKey publicKeyAdapter(String publicKeyStr, String kid) {
        PublicKey publicKey = null;
        if (!StringUtils.hasText(publicKeyStr)) {
            return null;
        }
        try {
            Map<String,List<Map<String,Object>>> maps =  JSON.parseObject(publicKeyStr,Map.class);
//            Map maps = (Map)JSON.parse(publicKeyStr);
            List<Map<String,Object>> keys = maps.get("keys");
            Map<String,Object> o = null;
            for (Map<String,Object> key : keys) {
                if (kid.equals(key.get("kid"))) {
                    o = key;
                    break;
                }
            }
            Jwk jwa = Jwk.fromValues(o);
            publicKey = jwa.getPublicKey();
        } catch (Exception e) {
            logger.error("publicKeyAdapter failed, kid:{}, publicKeyStr:{}", publicKey, kid, e);
        }
        return publicKey;
    }

    /**
     * 从appleServer获取publicKey
     *
     * @return String
     */
    private static String getAppleIdPublicKeyFromRemote() {
        ResponseEntity<String> responseEntity = REST_TEMPLATE.getForEntity(APPLEID_PUBLICKEY_URL, String.class);
        if (responseEntity == null || responseEntity.getStatusCode() != HttpStatus.OK) {
            logger.error(String.format("getAppleIdPublicKeyFromRemote [%s] exception, detail:", APPLEID_PUBLICKEY_URL));
            return null;
        }
        return responseEntity.getBody();
    }

    private static CusJws getJws(String identityToken) {
        String[] arrToken = identityToken.split("\\.");
        if (arrToken.length != 3) {
            return null;
        }
        Base64.Decoder decoder = Base64.getDecoder();
        JwsHeader jwsHeader = JSON.parseObject(new String(decoder.decode(arrToken[0])), JwsHeader.class);
        JwsPayload jwsPayload = JSON.parseObject(new String(decoder.decode(arrToken[1])), JwsPayload.class);
        return new CusJws(jwsHeader, jwsPayload, arrToken[2]);
    }

    static class CusJws {
        private JwsHeader jwsHeader;
        private JwsPayload jwsPayload;
        private String signature;

        public CusJws(JwsHeader jwsHeader, JwsPayload jwsPayload, String signature) {
            this.jwsHeader = jwsHeader;
            this.jwsPayload = jwsPayload;
            this.signature = signature;
        }

        public JwsHeader getJwsHeader() {
            return jwsHeader;
        }

        public void setJwsHeader(JwsHeader jwsHeader) {
            this.jwsHeader = jwsHeader;
        }

        public JwsPayload getJwsPayload() {
            return jwsPayload;
        }

        public void setJwsPayload(JwsPayload jwsPayload) {
            this.jwsPayload = jwsPayload;
        }

        public String getSignature() {
            return signature;
        }

        public void setSignature(String signature) {
            this.signature = signature;
        }

        @Override
        public String toString() {
            return JSON.toJSONString(this);
        }
    }

    static class JwsHeader {
        private String kid;
        private String alg;

        public String getKid() {
            return kid;
        }

        public void setKid(String kid) {
            this.kid = kid;
        }

        public String getAlg() {
            return alg;
        }

        public void setAlg(String alg) {
            this.alg = alg;
        }
    }

    static class JwsPayload {
        private String iss;
        private String sub;
        private String aud;
        private long exp;
        private long iat;
        private String nonce;
        private String email;
        private boolean email_verified;

        public String getIss() {
            return iss;
        }

        public void setIss(String iss) {
            this.iss = iss;
        }

        public String getSub() {
            return sub;
        }

        public void setSub(String sub) {
            this.sub = sub;
        }

        public String getAud() {
            return aud;
        }

        public void setAud(String aud) {
            this.aud = aud;
        }

        public long getExp() {
            return exp;
        }

        public void setExp(long exp) {
            this.exp = exp;
        }

        public long getIat() {
            return iat;
        }

        public void setIat(long iat) {
            this.iat = iat;
        }

        public String getNonce() {
            return nonce;
        }

        public void setNonce(String nonce) {
            this.nonce = nonce;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public boolean isEmail_verified() {
            return email_verified;
        }

        public void setEmail_verified(boolean email_verified) {
            this.email_verified = email_verified;
        }
    }

}
