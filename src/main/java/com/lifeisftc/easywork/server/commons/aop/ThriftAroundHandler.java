package com.lifeisftc.easywork.server.commons.aop;

import com.google.common.base.Joiner;
import com.lifeisftc.easywork.server.commons.common.StatusCode;
import com.lifeisftc.easywork.server.commons.exception.BusinessException;
import com.lifeisftc.easywork.server.commons.exception.ErrorBean;
import com.lifeisftc.easywork.server.commons.json.JsonMapper;
import com.lifeisftc.easywork.server.commons.metrics.InterfaceMonitor;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by yangyongxin on 2017/10/29.
 */
public class ThriftAroundHandler {

    Logger logger = LoggerFactory.getLogger(ThriftAroundHandler.class);
    Logger slowLogger = LoggerFactory.getLogger("SlowLogger");

    @Around(value = "execution (* com.didapinche..*.service..*(..))")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        //before
        Class clazz = pjp.getTarget().getClass();
        String methodName = String.format("%s.%s", clazz.getSimpleName(), pjp.getSignature().getName());
        logger.debug("----------" + methodName + " start------------");

        for (Object obj : pjp.getArgs()) {
            logger.debug(obj != null ? obj.toString() : "");
        }

        Object[] args = pjp.getArgs();
        ResultMap result = new ResultMap();
        long start = System.currentTimeMillis();

        try{
            InterfaceMonitor.getInstance().addTotal(methodName, InterfaceMonitor.TYPE_INTERFACE);
            result = (ResultMap) pjp.proceed(args);
            if (result != null && result.getCode() == 104) {
                InterfaceMonitor.getInstance().addFail(methodName, InterfaceMonitor.TYPE_INTERFACE);
                InterfaceMonitor.getInstance().setDuration(methodName, InterfaceMonitor.TYPE_INTERFACE, System.currentTimeMillis() - start, InterfaceMonitor.ERROR_FAIL);
                logger.error("{} error return. code:{} msg:{}  args:{}", methodName, result.getCode(), result.getMsg(), Joiner.on(",").skipNulls().join(args));
                logger.error("error message: {}", result.getMsg());
            } else if (result != null && result.getCode() != 0) {
                InterfaceMonitor.getInstance().addWarn(methodName, InterfaceMonitor.TYPE_INTERFACE);
                logger.info("{} illegal return. code:{} msg:{}  args:{}", methodName, result.getCode(), result.getMsg(), Joiner.on(",").skipNulls().join(args));
            }
            InterfaceMonitor.getInstance().setDuration(methodName, InterfaceMonitor.TYPE_INTERFACE, System.currentTimeMillis() - start, InterfaceMonitor.ERROR_SUCC);
        } catch (IllegalArgumentException e){
            ErrorBean errorBean = null;
            if(StringUtils.isNotEmpty(e.getMessage()) && e.getMessage().startsWith("{")) {
                errorBean = JsonMapper.json2Bean(e.getMessage(), ErrorBean.class);
            }
            //兼容message不为json的问题
            if (null != errorBean) {
                if (errorBean.getCode() != null && errorBean.getCode() == StatusCode.C_102) {
                    errorBean.setMessage(StatusCode.C_102_MSG);//统一设置成参数错误返回给app
                }
            } else {
                errorBean = new ErrorBean(StatusCode.C_102, StatusCode.C_102_MSG);
            }
            result.setCode(StatusCode.C_102);
            result.setMsg(StatusCode.C_102_MSG);

            logger.info(String.format("%s illegal argument. args:%s", methodName, Joiner.on(",").skipNulls().join(args)));
            logger.info(String.format("code&messge:%s", e.getMessage()));
            InterfaceMonitor.getInstance().addWarn(methodName, InterfaceMonitor.TYPE_INTERFACE);
            InterfaceMonitor.getInstance().setDuration(methodName, InterfaceMonitor.TYPE_INTERFACE, System.currentTimeMillis() - start, InterfaceMonitor.ERROR_SUCC);
        }catch (BusinessException e){
            if(e.getCode() == 104) {
                InterfaceMonitor.getInstance().addFail(methodName, InterfaceMonitor.TYPE_INTERFACE);
                InterfaceMonitor.getInstance().setDuration(methodName, InterfaceMonitor.TYPE_INTERFACE, System.currentTimeMillis() - start, InterfaceMonitor.ERROR_FAIL);
                logger.error("{} error return: SystemException. code:{} msg:{}  args:{}", methodName, result.getCode(), result.getMsg(), Joiner.on(",").skipNulls().join(args));
                logger.error("error message: {}", e);
            } else {
                InterfaceMonitor.getInstance().addWarn(methodName, InterfaceMonitor.TYPE_INTERFACE);
                InterfaceMonitor.getInstance().setDuration(methodName, InterfaceMonitor.TYPE_INTERFACE, System.currentTimeMillis() - start, InterfaceMonitor.ERROR_SUCC);
                logger.info("{} illegal return. code:{} msg:{}  args:{}", methodName, result.getCode(), result.getMsg(), Joiner.on(",").skipNulls().join(args));
            }
            result.setCode(Short.valueOf(String.valueOf(e.getCode())));
            result.setMsg(e.getMessage());

        }catch (Exception e){
            InterfaceMonitor.getInstance().addFail(methodName, InterfaceMonitor.TYPE_INTERFACE);
            InterfaceMonitor.getInstance().setDuration(methodName, InterfaceMonitor.TYPE_INTERFACE, System.currentTimeMillis() - start, InterfaceMonitor.ERROR_FAIL);
            logger.error("{} failed. code:{} msg:{}  args:{}", methodName, result.getCode(), result.getMsg(), Joiner.on(",").skipNulls().join(args));
            logger.error("error message: {}", e);
            result.setCode(Short.valueOf(String.valueOf(StatusCode.C_104)));
            result.setMsg(StatusCode.C_104_MSG);
        }

        long end = System.currentTimeMillis();
        if (end - start >= 100) {
            slowLogger.info(pjp.getTarget().getClass().getSimpleName()+"."+pjp.getSignature().getName() + " (cost " + (end - start) + " ms)------------");
            InterfaceMonitor.getInstance().addSlow(methodName, InterfaceMonitor.TYPE_INTERFACE);
        }

//        InterfaceMonitor.getInstance().printResultLog(methodName, InterfaceMonitor.INTERFACE_TYPE_THRIFT, result);

        logger.debug("result is {}", result.toString());
        //after
        logger.debug("----------" + methodName + " end------------");
        return result;
    }

}