package com.lifeisftc.easywork.server.commons.util;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.ByteArrayRequestEntity;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * @author gongyunlong
 */
public class FileUtil {
	private static Logger logger = LoggerFactory.getLogger(FileUtil.class);

	public static File createNewFile(String fileFullName) throws IOException{
		File file = new File(fileFullName);
		return createNewFile(file);
	}
	
	public static File createNewFile(File file) throws IOException{
		if (!file.exists()) {
			File parentDir = file.getParentFile();
			if(null != parentDir && !parentDir.exists()){
				parentDir.mkdirs();
			}
			file.createNewFile();
		}
		return file;
	}
	
	public static void createDirectory(File file) throws IOException{
		if (!file.exists()) {
			if (!file.mkdirs()) {
				logger.error("createDirectory failed. path=" + file.getAbsolutePath());				
			}
		}
	}
	
	public static String getFileMD5(File file){
		if (!file.isFile()) {
			return null;
		}
		MessageDigest digest = null;
		FileInputStream in = null;
		byte buffer[] = new byte[6144];
		int len;
		try {
			digest = MessageDigest.getInstance("MD5");
			in = new FileInputStream(file);
			while ((len = in.read(buffer)) != -1) {
				digest.update(buffer, 0, len);
			}
			BigInteger bigInt = new BigInteger(1, digest.digest());
			return bigInt.toString(16);
		} catch (Exception e) {
			logger.error("getFileMD5 failed.", e);
			return null;
		} finally {
			try {
				in.close();
			} catch (Exception e) {
			}
		}
	}
	/**
	 * 创建ZIP文件
	 * @param sourcePath 文件或文件夹路径
	 * @param zipName zip文件名称
	 */
	public static String createZip(String sourcePath, String zipName) {
		String zipPath = System.getProperty("java.io.tmpdir")+ File.separator + zipName + ".zip";
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		try {
			fos = new FileOutputStream(zipPath);
			zos = new ZipOutputStream(fos);
//            zos.setEncoding("gbk");//此处修改字节码方式。
			//createXmlFile(sourcePath,"293.xml");
			writeZip(new File(System.getProperty("java.io.tmpdir")+ File.separator +sourcePath), "", zos);
		} catch (FileNotFoundException e) {
			System.out.println("创建ZIP文件失败" + e);
		} finally {
			try {
				if (zos != null) {
					zos.close();
				}
			} catch (IOException e) {
				System.out.println("创建ZIP文件失败" + e);
			}
		}
		return zipPath;
	}

	private static void writeZip(File file, String parentPath, ZipOutputStream zos) {
		if (file.exists()) {
			if (file.isDirectory()) {
				//处理文件夹
				parentPath += file.getName() + File.separator;
				File[] files = file.listFiles();
				if (files.length != 0) {
					for (File f : files) {
						writeZip(f, parentPath, zos);
					}
				} else {
					//空目录则创建当前目录
					try {
						zos.putNextEntry(new ZipEntry(parentPath));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				FileInputStream fis = null;
				try {
					fis = new FileInputStream(file);
					ZipEntry ze = new ZipEntry(parentPath + file.getName());
					zos.putNextEntry(ze);
					byte[] content = new byte[1024];
					int len;
					while ((len = fis.read(content)) != -1) {
						zos.write(content, 0, len);
						zos.flush();
					}

				} catch (FileNotFoundException e) {
					System.out.println("创建ZIP文件失败" + e);
				} catch (IOException e) {
					System.out.println("创建ZIP文件失败" + e);
				} finally {
					try {
						if (fis != null) {
							fis.close();
						}
					} catch (IOException e) {
						System.out.println("创建ZIP文件失败" + e);
					}
				}
			}
		}
	}

	/**
	 * 解压文件到descdir
	 * @param zipFile
	 * @param descDir
	 * @return
	 */
	public static List<File> unZipFileGetFileLists(File zipFile, String descDir){
		try {
			byte[] _byte = new byte[1024] ;
			List<File> _list = new ArrayList<File>() ;
			ZipFile zip = new ZipFile(zipFile, Charset.forName("GBK"));//解决中文文件夹乱码

			for (Enumeration<? extends ZipEntry> entries = zip.entries(); entries.hasMoreElements();) {
				ZipEntry entry = (ZipEntry) entries.nextElement();
				File _file = new File(descDir + File.separator + entry.getName()) ;
				if( entry.isDirectory() ){
					_file.mkdirs() ;
				}else{
					File _parent = _file.getParentFile() ;
					if( !_parent.exists() ){
						_parent.mkdirs() ;
					}
					InputStream _in = zip.getInputStream(entry);
					OutputStream _out = new FileOutputStream(_file) ;
					int len = 0 ;
					while( (len = _in.read(_byte)) > 0){
						_out.write(_byte, 0, len);
					}
					_in.close();
					_out.flush();
					_out.close();
					_list.add(_file) ;
				}
			}
			logger.info("解压完毕:filename:"+zipFile.getName());
			zip.close();
			zipFile.delete();
			return _list;
		}catch (IOException ie){
			logger.error("FileUtil.unZipFileGetFileLists is io error",ie);
			throw new RuntimeException("zip 解压失败");
		}catch (Exception e){
			logger.error("FileUtil.unZipFileGetFileLists is error",e);
			throw new RuntimeException("zip 解压失败");
		}
	}

	/**
	 * 删除文件或文件目录
	 * @param dir
	 * @return
	 */
	public static boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			//递归删除目录中的子目录下
			for (int i=0; i<children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		// 目录此时为空，可以删除
		return dir.delete();
	}

	/**
	 * 获取文件格式
	 * @param filename
	 * @return
	 */
	public static String getExtensionName(String filename) {
		String exName = "";
		if ((filename != null) && (filename.length() > 0)) {
			int dot = filename.lastIndexOf('.');
			if ((dot >-1) && (dot < (filename.length() - 1))) {
				exName =  filename.substring(dot);
			}
		}
		return exName;
	}

	/**
	 * 拷贝文件 默认保存路径在java.io.tmpdir
	 * @param file 原文件
	 * @param fileName 拷贝文件名
	 * @return
	 * @throws IOException
	 */
	public static File copyFile(File file, String fileName) throws IOException {
		FileInputStream fis = new FileInputStream(file);

		File copyFile = new File(System.getProperty("java.io.tmpdir"), fileName);
		FileOutputStream fos = new FileOutputStream(copyFile);

		byte[] bts = new byte[2048];
		int len = 0;
		while((len = fis.read(bts)) != -1){
			fos.write(bts,0,len);
		}
		fis.close();
		fos.close();

		return copyFile;
	}

	/**
	 * 拷贝文件
	 * @param file 原文件
	 * @param fileName 拷贝文件完整路径
	 * @return
	 * @throws IOException
	 */
	public static File copyFileAbsolute(File file, String fileName) throws IOException {
		FileInputStream fis = new FileInputStream(file);

		File copyFile = new File(fileName);
		FileOutputStream fos = new FileOutputStream(copyFile);

		byte[] bts = new byte[2048];
		int len = 0;
		while((len = fis.read(bts)) != -1){
			fos.write(bts,0,len);
		}
		fis.close();
		fos.close();
		return copyFile;
	}

	/**
	 * 下载文件
	 * @param remoteFilePath
	 * @param localFileName
	 * @return
	 */
	public static String downloadFile(String remoteFilePath, String localFileName) {
		HttpClient client = new HttpClient();
		GetMethod get = null;
		InputStream in = null;
		OutputStream out = null;
		String filePath = null;

		try {
			get = new GetMethod(remoteFilePath);
			get.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,"GB2312");
			get.addRequestHeader("Content-Type", "text/html; charset=gb2312");

			int status = client.executeMethod(get);
			if (status == HttpStatus.SC_OK) {
				logger.info("get file return 200,status:"+status+",url:"+remoteFilePath);

				in = new BufferedInputStream(get.getResponseBodyAsStream(),1024);
				File storeFile = new File(System.getProperty("java.io.tmpdir"), localFileName);
				out = new BufferedOutputStream(new FileOutputStream(storeFile), 1024);

				byte[] b = new byte[1024];
				int len = 0;
				while (-1 != (len = in.read(b))) {
					out.write(b, 0, len);
				}
				filePath = storeFile.getAbsolutePath();
			} else {
				logger.info("get file not return 200,status:"+status+",url:"+remoteFilePath);
			}
		} catch (Exception e) {
			logger.error("get file failed,url:"+remoteFilePath,e);
		} finally {
			try {
				if (null != in) {
					in.close();
				}
				if (null != out) {
					out.close();
				}
			} catch (IOException e) {
				logger.error("close stream failed",e);
			}

			if (get != null) {
				get.releaseConnection();
			}
			client.getHttpConnectionManager().closeIdleConnections(0);
		}
		return filePath;
	}

	/**
	 * 上传文件到服务器 字节流
	 * */
	public static String uploadFile(byte[] fileBytes, String url,String permission) throws Exception {
		HttpClient httpClient = new HttpClient();
		PutMethod putMethod = new PutMethod(url);
		putMethod.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		putMethod.setRequestHeader("Permission",permission);

		putMethod.setRequestEntity(new ByteArrayRequestEntity(fileBytes));

		httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
//        HttpConnectionManagerParams managerParams = httpClient.getHttpConnectionManager().getParams();
//        // 设置连接超时时间(单位毫秒)
//        managerParams.setConnectionTimeout(10000);
//        // 设置读数据超时时间(单位毫秒)
//        managerParams.setSoTimeout(10000);

		int status = httpClient.executeMethod(putMethod);
		if (status == HttpStatus.SC_OK) {
			Header responseHeader = putMethod.getResponseHeader("Location");
			String location = "";
			if (responseHeader != null) {
				location = responseHeader.getValue();
			} else { // 文件没有成功保存，需要重新上传
				logger.info("file save failed.");
				return null;
			}
			logger.info("file upload success, location:" + location);
			return location;
		} else {
			logger.error("file uploadFile error url:{},status:{}", url,status);
			throw new Exception("服务器错误");
		}
	}

	/**
	 * 删除服务器文件
	 * @param url
	 */
	public static String deleteFile(String url) {
		HttpClient httpClient = new HttpClient();
		DeleteMethod deleteMethod = new DeleteMethod(url);

		httpClient.getHttpConnectionManager().getParams()
				.setConnectionTimeout(5000);
		try {
			int status = httpClient.executeMethod(deleteMethod);
			if (status == HttpStatus.SC_OK) {
				logger.info("delete file return HttpStatus.SC_OK,status:"+status+",url:"+url);
			} else {
				logger.info("delete file not return HttpStatus.SC_OK,status:"+status+",url:"+url);
			}
		} catch (Exception e) {
			logger.error("delete file failed,url:"+url,e);
		} finally {
			deleteMethod.releaseConnection();
		}
		return null;
	}

	/**
	 * 读取本地文件
	 */
	public static byte[] readLocalFile(String localFilePath){
		InputStream ins = FileUtil.class.getClassLoader().getResourceAsStream(localFilePath);
		byte[] bytes = null;
		try {
			int length = ins.available();
			bytes = new byte[length];
			ins.read(bytes);
		}catch (IOException e){
			logger.error("readLocalFile failed,localFilePath:"+localFilePath,e);
		}finally {
			try{
				if(null != ins) {
					ins.close();
				}
			}catch (IOException e){
				logger.error("readLocalFile close stream failed",e);
			}
		}
		return bytes;
	}
	
}
