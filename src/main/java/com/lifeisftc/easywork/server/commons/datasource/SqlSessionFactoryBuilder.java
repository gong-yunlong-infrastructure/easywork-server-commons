package com.lifeisftc.easywork.server.commons.datasource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;

/**
 * Created by yangyongxin on 2018/2/28.
 */
public class SqlSessionFactoryBuilder {
    private final Logger logger = LoggerFactory.getLogger(SqlSessionFactoryBuilder.class);

    public SqlSessionFactory build(DataSource dataSource) throws Exception {
        return this.build(dataSource, false);
    }
    public SqlSessionFactory build(DataSource dataSource, boolean isMaster) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        if (isMaster){
            bean.setTransactionFactory(new CustomSpringManagedTransactionFactory());
        }
        try {
            return bean.getObject();
        } catch (Exception e) {
            logger.error("SqlSessionFactoryBuilder get sqlSessionFactory failed.", e);
            throw e;
        }
    }
}
