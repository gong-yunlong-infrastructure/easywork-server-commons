package com.lifeisftc.easywork.server.commons.aop;


import com.lifeisftc.easywork.server.commons.common.StatusCode;
import com.lifeisftc.easywork.server.commons.json.JsonMapper;

import java.util.HashMap;
import java.util.Map;

/**
 * @author gongyunlong
 */
public class ResultMap {
    private Map<String, Object> map = new HashMap<>();

    public int getCode() {
        return map.get("code") == null ? -1 : (Integer)map.get("code") ;
    }

    public ResultMap setCode(int code) {
        map.put("code", code);
        return this;
    }
    public ResultMap(){
        map.put("code", StatusCode.C_SUCCESS);
        map.put("msg", "");
    }

    public String getMsg() {
        return map.get("msg") == null ? "" : (String)map.get("msg");
    }

    public ResultMap setMsg(String msg) {
        map.put("msg", msg);
        return this;
    }

    public ResultMap put(String key, Object obj) {
        map.put(key, obj);
        return this;
    }

    public Object get(String key) {
        return map.get(key);
    }


    /**
     * 构造操作失败ReplyEntity对象
     */
    public ResultMap fail() {
        this.fail(StatusCode.C_104, StatusCode.C_104_MSG);
        return this;
    }

    /**
     * 构造操作失败ReplyEntity对象
     */
    public ResultMap fail(int code , String message) {
        map.put("code", code);
        map.put("msg", message);
        return this;
    }

    public String toJson() {
        return JsonMapper.toJson(map);
    }

    @Override
    public String toString() {
        return toJson();
    }

    public <T> T convertTo(Class<T> clazz) {
        return JsonMapper.json2Bean(JsonMapper.toJson(map), clazz);
    }
}
