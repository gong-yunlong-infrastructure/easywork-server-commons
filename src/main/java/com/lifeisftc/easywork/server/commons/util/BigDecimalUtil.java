package com.lifeisftc.easywork.server.commons.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author gongyunlong
 */
public class BigDecimalUtil {

	//四舍五入
	public static final int ROUND_HALF_UP = BigDecimal.ROUND_HALF_UP;

	public static Double toDouble(BigDecimal obj){
		if(obj!=null){
			return obj.doubleValue();
		}else{
			return 0d;
		}
	}
	
	public static BigDecimal nullHandle(BigDecimal obj){
		if(obj!=null){
			return obj;
		}else{
			return new BigDecimal("0");
		}
	}

	public static BigDecimal getDivide(int obj,int div){
		return new BigDecimal(obj).divide(new BigDecimal(div)).setScale(1, RoundingMode.HALF_UP);
	}

	public static float format(BigDecimal num) {
		if(num == null){
			return 0f;
		}
		return num.setScale(2, ROUND_HALF_UP).floatValue();
	}

	public static String toString(BigDecimal num){
		if(num == null){
			return "0";
		}
		return num.setScale(2, ROUND_HALF_UP).toString();
	}

	public static String toStringLeft1(BigDecimal num){
		if(num == null){
			return "0.0";
		}
		return num.setScale(1, ROUND_HALF_UP).toString();
	}

	public static String toStringLeft2(BigDecimal num){
		if(num == null){
			return "0.00";
		}
		return num.setScale(2, ROUND_HALF_UP).toString();
	}

	public static BigDecimal float2BigDecimal(Float floatValue,int left){
		if (floatValue==null){
			return new BigDecimal(0);
		}
		return (new BigDecimal(floatValue)).setScale(left, ROUND_HALF_UP);
	}
}
