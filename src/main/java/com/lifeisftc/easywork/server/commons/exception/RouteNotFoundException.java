package com.lifeisftc.easywork.server.commons.exception;

/**
 * Created by leiyang on 18-7-31.
 */
public class RouteNotFoundException extends Exception {

    public RouteNotFoundException() {
    }

    public RouteNotFoundException(String message) {
        super(message);
    }

    public RouteNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public RouteNotFoundException(Throwable cause) {
        super(cause);
    }
}
