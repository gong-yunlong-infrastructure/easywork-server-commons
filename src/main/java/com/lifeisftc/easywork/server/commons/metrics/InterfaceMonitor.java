package com.lifeisftc.easywork.server.commons.metrics;

import com.lifeisftc.common.lang.executor.ExecutorHelper;
import com.lifeisftc.easywork.server.commons.metrics.entity.InterfaceDimensionEntity;
import com.lifeisftc.easywork.server.commons.metrics.proxy.MonitorObserverProxy;
import com.lifeisftc.easywork.server.commons.util.LoadPropertyUtil;
import io.prometheus.client.hotspot.DefaultExports;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author gongyunlong
 */
public class InterfaceMonitor {
    private static Logger logger = LoggerFactory.getLogger(InterfaceMonitor.class);

    public static boolean enable = false;

    public static final String TYPE_INTERFACE = "interface";
    public static final String TYPE_REDIS = "redis";
    public static final String TYPE_REDIS_CLUSTER = "redis_cluster";
    public static final String TYPE_MEMCACHE = "memcache";
    public static final String TYPE_ROCKETMQ = "rocketmq";
    public static final String TYPE_PUSH = "push";
    public static final String TYPE_CUSTOM = "custom";

    public static final String ERROR_SUCC = "success";
    public static final String ERROR_FAIL = "fail";

    public static final String INTERFACE_TYPE_RS = "rs";
    public static final String INTERFACE_TYPE_THRIFT = "thrift";

    private static InterfaceDimensionEntity THIS_MIN_DATA;
    private static InterfaceDimensionEntity LAST_MIN_DATA;

    private static InterfaceMonitor instance = null;
//    private Counter errorPoint = Counter.build().name("server_rs_error_code").help("error code info").labelNames("app", "endpoint", "method", "type", "code", "message").register();

    public static void initialize() {
        synchronized (InterfaceMonitor.class) {
            if(instance == null) {
                enable = StringUtils.isBlank(LoadPropertyUtil.getProperty("monitor.enable")) || "1".equals(LoadPropertyUtil.getProperty("monitor.enable"));
                instance = new InterfaceMonitor();
                if(enable) {
                    THIS_MIN_DATA = new InterfaceDimensionEntity();
                    LAST_MIN_DATA = new InterfaceDimensionEntity(false);

                    ScheduledExecutorService scheduledExecutor = ExecutorHelper.getInstance().getScheduledExecutor("scheduledExecutor");
                    scheduledExecutor.scheduleAtFixedRate(() -> {
                        try {
                            InterfaceDimensionEntity copyThis = THIS_MIN_DATA.getCopy();
                            InterfaceDimensionEntity copyLast = LAST_MIN_DATA.getCopy();
                            LAST_MIN_DATA.copy(THIS_MIN_DATA);
                            THIS_MIN_DATA.clear();
                            remindObserver(copyThis, copyLast);
                        } catch (Exception e) {
                            logger.error("remind observer error", e);
                        }
                    }, 1, 1, TimeUnit.MINUTES);
                    MonitorObserverProxy.init();
                    DefaultExports.initialize();
                }
            }
        }
    }

    public static InterfaceMonitor getInstance() {
        if(instance == null) {
            synchronized (InterfaceMonitor.class) {
                if(instance == null) {
                    initialize();
                }
            }
        }
        return instance;
    }

    public void addTotal(String method, String type){
        if(!enable) {
            return;
        }
        try {
            THIS_MIN_DATA.addTotal(method, type);
        } catch (Exception e) {
            logger.error("InterfaceMonitor addTotal error", e);
        }
    }

    public void addFail(String method, String type) {
        if(!enable) {
            return;
        }
        try {
            THIS_MIN_DATA.addFail(method, type);
        } catch (Exception e) {
            logger.error("InterfaceMonitor addFail error", e);
        }
    }

    public void addSlow(String method, String type){
        if(!enable) {
            return;
        }
        try {
            THIS_MIN_DATA.addSlow(method, type);
        } catch (Exception e) {
            logger.error("InterfaceMonitor addSlow error", e);
        }
    }

    public void addWarn(String method, String type){
        if(!enable) {
            return;
        }
        try {
            THIS_MIN_DATA.addWarn(method, type);
        } catch (Exception e) {
            logger.error("InterfaceMonitor addWarn error", e);
        }
    }

    /**
     * 默认时间单位毫秒
     */
    public void setDuration(String method, String type, double time, String error){
        if(!enable) {
            return;
        }
        try {
            THIS_MIN_DATA.setDuration(method, type, time / 1000D, error);
        } catch (Exception e) {
            logger.error("InterfaceMonitor setDuration error", e);
        }
    }

    public void setDuration(String method, String type, long time, TimeUnit unit, String error){
        if(!enable) {
            return;
        }
        try {
            THIS_MIN_DATA.setDuration(method, type, TimeUnit.MILLISECONDS.convert(time, unit) / 1000D, error);
        } catch (Exception e) {
            logger.error("InterfaceMonitor setDuration error", e);
        }
    }

    public static void remindObserver(InterfaceDimensionEntity now, InterfaceDimensionEntity past) {
        if(!enable) {
            return;
        }
        MonitorObserverProxy.update(now, past);
    }

//    public void printResultLog(String methodName, String type, Object result) {
//        if(result == null) {
//            return;
//        }
//        String resultstr = String.valueOf(result);
//        if(org.apache.commons.lang.StringUtils.isBlank(resultstr)) {
//            return;
//        }
//        Map res = JsonMapper.json2Map(resultstr);
//        if(res == null) {
//            return;
//        }
//        Integer code = (Integer) res.get("code");
//        String message = (String) res.get("message");
//        errorPoint.labels(CommonUtils.getAppName(), CommonUtils.getHostName(), methodName, type, "" + code, "" + message).inc();
//    }
}
