package com.lifeisftc.easywork.server.commons.metrics.entity;


import com.lifeisftc.easywork.server.commons.metrics.base.CommonUtils;
import io.prometheus.client.Counter;
import io.prometheus.client.Histogram;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 接口类型统计实体类
 * @author 巩云龙
 */
public class InterfaceDimensionEntity {
    private ConcurrentHashMap<String, DimensionStatisticsEntity> total = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, DimensionStatisticsEntity> fail = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, DimensionStatisticsEntity> slow = new ConcurrentHashMap<>();
    private ConcurrentHashMap<String, DimensionStatisticsEntity> warn = new ConcurrentHashMap<>();

    private Counter totalCounter;
    private Counter failCounter;
    private Counter slowCounter;
    private Counter warnCounter;
    private Histogram duration;

    public InterfaceDimensionEntity() {
        initCounter();
    }

    public InterfaceDimensionEntity(boolean init) {
        if(init) {
            initCounter();
        }
    }

    public void initCounter() {
        totalCounter = Counter.build().name("server_realtime_total_count").help("realtime call total num count").labelNames("app", "endpoint", "method", "type").register();
        failCounter = Counter.build().name("server_realtime_fail_count").help("realtime call fail num count").labelNames("app", "endpoint", "method", "type").register();
        slowCounter = Counter.build().name("server_realtime_slow_count").help("realtime call slow num count").labelNames("app", "endpoint", "method", "type").register();
        warnCounter = Counter.build().name("server_realtime_warn_count").help("realtime call warn num count").labelNames("app", "endpoint", "method", "type").register();
        duration = Histogram.build().name("server_duration").help("interface duration").labelNames("app", "endpoint", "method", "type", "error").register();
    }

    public void setDuration(String method, String type, double time, String error) {
        duration.labels(CommonUtils.getAppName(), CommonUtils.getHostName(), method, type, error).observe(time);
    }

    public void addTotal(String method, String type) {
        if(total.get(method) == null) {
            synchronized (InterfaceDimensionEntity.class) {
                if(total.get(method) == null) {
                    total.put(method, new DimensionStatisticsEntity(method, type));
                }
            }
        }
        total.get(method).call();
        totalCounter.labels(CommonUtils.getAppName(), CommonUtils.getHostName(), method, type).inc();
    }

    public void addFail(String method, String type) {
        if(fail.get(method) == null) {
            synchronized (InterfaceDimensionEntity.class) {
                if(fail.get(method) == null) {
                    fail.put(method, new DimensionStatisticsEntity(method, type));
                }
            }
        }
        fail.get(method).call();
        failCounter.labels(CommonUtils.getAppName(), CommonUtils.getHostName(), method, type).inc();
    }
    public void addSlow(String method, String type) {
        if(slow.get(method) == null) {
            synchronized (InterfaceDimensionEntity.class) {
                if(slow.get(method) == null) {
                    slow.put(method, new DimensionStatisticsEntity(method, type));
                }
            }
        }
        slow.get(method).call();
        slowCounter.labels(CommonUtils.getAppName(), CommonUtils.getHostName(), method, type).inc();
    }
    public void addWarn(String method, String type) {
        if(warn.get(method) == null) {
            synchronized (InterfaceDimensionEntity.class) {
                if(warn.get(method) == null) {
                    warn.put(method, new DimensionStatisticsEntity(method, type));
                }
            }
        }
        warn.get(method).call();
        warnCounter.labels(CommonUtils.getAppName(), CommonUtils.getHostName(), method, type).inc();
    }

    public ConcurrentHashMap<String, DimensionStatisticsEntity> getTotalCopy() {
        if(total == null) {
            return null;
        }
        ConcurrentHashMap<String, DimensionStatisticsEntity> copy = new ConcurrentHashMap<>();
        copy.putAll(total);
        return copy;
    }
    public ConcurrentHashMap<String, DimensionStatisticsEntity> getFailCopy() {
        if(fail == null) {
            return null;
        }
        ConcurrentHashMap<String, DimensionStatisticsEntity> copy = new ConcurrentHashMap<>();
        copy.putAll(fail);
        return copy;
    }
    public ConcurrentHashMap<String, DimensionStatisticsEntity> getSlowCopy() {
        if(slow == null) {
            return null;
        }
        ConcurrentHashMap<String, DimensionStatisticsEntity> copy = new ConcurrentHashMap<>();
        copy.putAll(slow);
        return copy;
    }
    public ConcurrentHashMap<String, DimensionStatisticsEntity> getWarnCopy() {
        if(warn == null) {
            return null;
        }
        ConcurrentHashMap<String, DimensionStatisticsEntity> copy = new ConcurrentHashMap<>();
        copy.putAll(warn);
        return copy;
    }

    public void copy(InterfaceDimensionEntity entity) {
        this.total = entity.getTotalCopy();
        this.fail = entity.getFailCopy();
        this.slow = entity.getSlowCopy();
        this.warn = entity.getWarnCopy();
    }

    public void clear() {
        this.total = new ConcurrentHashMap<>();
        this.fail = new ConcurrentHashMap<>();
        this.slow = new ConcurrentHashMap<>();
        this.warn = new ConcurrentHashMap<>();
    }

    public InterfaceDimensionEntity getCopy() {
        InterfaceDimensionEntity data = new InterfaceDimensionEntity(false);
        data.copy(this);
        return data;
    }
}
