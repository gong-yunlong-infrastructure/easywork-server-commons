package com.lifeisftc.easywork.server.commons.util;

//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;


/**
 * @author gongyunlong
 */
public class AesCBC {

//    private static final String S_KEY = "eYpkvHTfHmmg4@Cv";
//    private static final String S_IV = "eYpkvHTfHmmg4@Cv";
//    private static AesCBC instance = null;
//
//    private AesCBC() {
//
//    }
//
//    public static AesCBC getInstance() {
//        if (instance == null) {
//            instance = new AesCBC();
//        }
//        return instance;
//    }
//
//    /**
//     * CBC加密函数
//     * @param sSrc 要加密的字符串
//     * @param encodingFormat 编码格式
//     * @param sKey sKey
//     * @param ivParameter iv
//     * @return String
//     * @throws Exception Exception
//     */
//    public String encrypt(String sSrc, String encodingFormat, String sKey, String ivParameter) throws Exception {
//        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//        byte[] raw = sKey.getBytes();
//        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//        //使用CBC模式，需要一个向量iv，可增加加密算法的强度
//        IvParameterSpec iv = new IvParameterSpec(ivParameter.getBytes());
//        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
//        byte[] encrypted = cipher.doFinal(sSrc.getBytes(encodingFormat));
//        //此处使用BASE64做转码
//        return new BASE64Encoder().encode(encrypted);
//    }
//
//    /**
//     * CBC解密函数
//     * @param sSrc 要解密的字符串
//     * @param encodingFormat 编码格式
//     * @param sKey String
//     * @param ivParameter String
//     * @return String
//     * @throws Exception Exception
//     */
//    public String decrypt(String sSrc, String encodingFormat, String sKey, String ivParameter) throws Exception {
//        try {
//            byte[] raw = sKey.getBytes("ASCII");
//            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
//            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//            IvParameterSpec iv = new IvParameterSpec(ivParameter.getBytes());
//            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
//            //先用base64解密
//            byte[] encrypted1 = new BASE64Decoder().decodeBuffer(sSrc);
//            byte[] original = cipher.doFinal(encrypted1);
//            return new String(original, encodingFormat);
//        } catch (Exception ex) {
//            return null;
//        }
//    }
//
//    public static void main(String[] args) throws Exception {
//        String tmp = "xGJ8UCfnjSm4PqvRGkepY+pXw+oQejhVLNk482wq5ja3BaOJqtLPx9KjfRQh4RzKhCc2Wo/5dSsseWGfIjfV8jQCe0gOkPf3G4JWBQ8/eIU=";
//
//        // 需要加密的字串
//        String cSrc = "{\"source\":\"10000\",\"requestId\":\"c7ade266-e087-4375-a004-5969c9466257\"}";
//        System.out.println(cSrc);
//        // 加密
//        long lStart = System.currentTimeMillis();
//        String enString = AesCBC.getInstance().encrypt(cSrc, "utf-8", S_KEY, S_IV);
//        System.out.println("加密后的字串是：" + enString);
//        System.out.println(enString.equals(tmp));
//
//        long lUseTime = System.currentTimeMillis() - lStart;
//        System.out.println("加密耗时：" + lUseTime + "毫秒");
//        // 解密
//        lStart = System.currentTimeMillis();
//        String deString = AesCBC.getInstance().decrypt(enString, "utf-8", S_KEY, S_IV);
//        System.out.println("解密后的字串是：" + deString);
//        System.out.println(deString.equals(cSrc));
//        lUseTime = System.currentTimeMillis() - lStart;
//        System.out.println("解密耗时：" + lUseTime + "毫秒");
//    }
}

