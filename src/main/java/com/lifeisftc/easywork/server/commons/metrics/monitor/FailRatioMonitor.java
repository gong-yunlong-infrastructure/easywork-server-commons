package com.lifeisftc.easywork.server.commons.metrics.monitor;

import com.lifeisftc.easywork.server.commons.metrics.IMonitorObserver;
import com.lifeisftc.easywork.server.commons.metrics.base.CommonUtils;
import com.lifeisftc.easywork.server.commons.metrics.entity.DimensionStatisticsEntity;
import com.lifeisftc.easywork.server.commons.metrics.entity.InterfaceDimensionEntity;
import io.prometheus.client.Gauge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 失败率统计
 * @author gongyunlong
 */
public class FailRatioMonitor implements IMonitorObserver {
    private static Logger logger = LoggerFactory.getLogger(FailRatioMonitor.class);

    private static Gauge gauge = Gauge.build().name("server_fail_ratio").help("about fail ratio").labelNames("app", "endpoint", "method", "type").register();

    public FailRatioMonitor() {
        logger.info("FailRatioMonitor init...");
    }

    @Override
    public String name() {
        return "FailRatioMonitor";
    }

    @Override
    public void update(InterfaceDimensionEntity now, InterfaceDimensionEntity past) {
        if(now.getTotalCopy() == null) {
            return;
        }

        for(Map.Entry<String, DimensionStatisticsEntity> entry: now.getTotalCopy().entrySet()) {
            BigDecimal totalnum = new BigDecimal(entry.getValue().getCount());
            if(totalnum.subtract(new BigDecimal(10)).longValue() < 0) {
                //总调用次数小于10次，略过
                continue;
            }
            if(now.getFailCopy() == null || now.getFailCopy().get(entry.getKey()) == null) {
                continue;
            }

            BigDecimal failnum = new BigDecimal(now.getFailCopy().get(entry.getKey()).getCount());
            BigDecimal ratio = failnum.divide(totalnum, 2, BigDecimal.ROUND_DOWN).multiply(new BigDecimal(100));
            gauge.labels(CommonUtils.getAppName(), CommonUtils.getHostName(), entry.getKey().replaceAll("\\.", "_"), entry.getValue().getType()).set(ratio.setScale(2).doubleValue());
        }
    }
}
