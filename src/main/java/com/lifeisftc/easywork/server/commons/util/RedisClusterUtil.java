package com.lifeisftc.easywork.server.commons.util;

import com.lifeisftc.common.tx.ITxCommittedCallback;
import com.lifeisftc.common.tx.TransactionExtHelper;
import com.lifeisftc.easywork.server.commons.json.JsonMapper;
import com.lifeisftc.easywork.server.commons.metrics.InterfaceMonitor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.ScanParams;
import redis.clients.jedis.ScanResult;
import redis.clients.jedis.Tuple;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

public class RedisClusterUtil implements ApplicationContextAware {

    private static Logger logger = LoggerFactory.getLogger(RedisClusterUtil.class);
    private static ApplicationContext context;

    private static Map<String, JedisCluster> clientMap = new ConcurrentHashMap<>();
    public static final String BEAN_PREFIX = "redisCluster";

    public static JedisCluster getRedisClient(String groupKey, Object key){
        String keyString = groupKey + key;
        if (StringUtils.isEmpty(keyString) || keyString.length() < 5) {
            return null;
        }

        String cluster = keyString.substring(0, keyString.indexOf("]") + 1);
        if(!clientMap.containsKey(cluster)){
            clientMap.put(cluster, (JedisCluster) context.getBean(RedisClusterUtil.BEAN_PREFIX + cluster.replaceAll("\\[rc(.*)\\]", "$1")));
        }

        JedisCluster jedisCluster = clientMap.get(keyString.substring(0, keyString.indexOf("]") + 1));
        if (Objects.isNull(jedisCluster)){
            throw new NullPointerException("cluster is not found ,please check your params");
        }
        return jedisCluster;
    }

    /**
     * redis set命令
     *
     * @param groupKey
     * @param key
     * @param value
     * @param seconds  过期倒计时的秒数(－1代表永不过期)
     * @since 4.3
     */
    public static void setAndExpire(final String groupKey, final Object key, final Object value, final int seconds, boolean isTx) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                String redisKey = String.format("%s%s", groupKey, key);
                try {
                    getRedisClient(groupKey, key).set(redisKey, JsonMapper.toJson(value));
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(redisKey, seconds);
                    }
                    LoggerUtil.logSlow(start, 100, "RedisClusterUtil.setAndExpire " + redisKey);
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.set failed. groupKey:{} key:{} Object:{}", groupKey, key, JsonMapper.toJson(value), e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.setAndExpire.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
            };
        if (isTx) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
    }


    /**
     * redis set命令
     *
     * @param groupKey
     * @param key
     * @param value
     * @param cal      指定的过期日期(例如：2016-06-06 23:59:59过期)
     * @since 4.3
     */
    public static void setAndExpireAt(final String groupKey, final Object key, final Object value, final Calendar cal, boolean isTx) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                String redisKey = String.format("%s%s", groupKey, key);
                try {
                    getRedisClient(groupKey, key).set(redisKey, JsonMapper.toJson(value));
                    getRedisClient(groupKey, key).expireAt(redisKey, (long) (cal.getTimeInMillis() / 1000.0));
                    LoggerUtil.logSlow(start, 100, "RedisClusterUtil.setAndExpireAt " + redisKey);
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.set failed. groupKey:{} key:{} Object:{}", groupKey, key, JsonMapper.toJson(value), e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.setAndExpireAt.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };

        if (isTx) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
    }


    /**
     * 放入Redis，当天有效
     *
     * @param groupKey
     * @param key
     * @param value
     * @since 4.3
     */
    public static void setToday(final String groupKey, final Object key, final Object value, boolean isTx) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, 24);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);

        setAndExpireAt(groupKey, key, value, cal, isTx);

    }

    /**
     * 取得clazz类型的对象
     *
     * @param groupKey
     * @param key
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T get(final String groupKey, final Object key, final Class<T> clazz) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        final String redisKey = String.format("%s%s", groupKey, key);
        String result;
        try {
            result = getRedisClient(groupKey, key).get(redisKey);
            LoggerUtil.logSlow(start, 100, "RedisClusterUtil.get " + redisKey);
        } catch (Exception e) {
            logger.error("RedisClusterUtil.get failed. groupKey:{} key:{}", groupKey, key, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.get.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return JsonMapper.json2Bean(result, clazz);

    }

    public static <T> T get(final String groupKey, final Object key, final Type type) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        final String redisKey = String.format("%s%s", groupKey, key);
        String result;
        try {
            result = RedisClusterUtil.getRedisClient(groupKey, key).get(redisKey);
            LoggerUtil.logSlow(start, 100, "RedisClusterUtil.get " + redisKey);
        } catch (Exception e) {
            logger.error("RedisClusterUtil.get failed. groupKey:{} key:{}", groupKey, key, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.get.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return JsonMapper.json2Bean(result,type);
    }

    /**
     * 取得clazz类型的List对象
     *
     * @param groupKey
     * @param key
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> getList(final String groupKey, final Object key, final Class<T> clazz) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        final String redisKey = String.format("%s%s", groupKey, key);
        String result;
        try {
            result = RedisClusterUtil.getRedisClient(groupKey, key).get(redisKey);
            LoggerUtil.logSlow(start, 100, "RedisClusterUtil.getList " + redisKey);
        }catch (Exception e) {
            logger.error("RedisClusterUtil.getList failed. groupKey:{} key:{}", groupKey, key, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.getList.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return JsonMapper.json2List(result, clazz);

    }

    /**
     * hash方式放入redis
     *
     * @param groupKey
     * @param key
     * @param field    放入字段名
     * @param value    toJson
     */
    public static void hset(final String groupKey, final Object key, final String field, final Object value, final int seconds) {
        hset(groupKey, key, field, value, seconds, true);
    }

    /**
     * hash方式放入redis
     *
     * @param groupKey
     * @param key
     * @param field    放入字段名
     * @param value    toJson
     */
    public static void hset(final String groupKey, final Object key, final String field, final Object value, final int seconds, boolean isTx) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }
        final String redisKey = String.format("%s%s", groupKey, key);
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    getRedisClient(groupKey, key).hset(redisKey, field, JsonMapper.toJson(value));
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(redisKey, seconds);
                    }
                    LoggerUtil.logSlow(start, 100, "RedisClusterUtil.hset " + redisKey);
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.hset failed. groupKey = " + groupKey
                            + "key = " + key
                            + " field = " + field
                            + " Object = " + JsonMapper.toJson(value), e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.hset.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };

        if (isTx) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }

    }

    /**
     * @param groupKey
     * @param key
     * @param value
     * @param seconds  过期倒计时的秒数(－1代表永不过期)
     * @since v4.3
     */
    public static void hmset(final String groupKey, final Object key, final Object value, final int seconds, boolean isTx) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }
        try {
            if (isTx && TransactionExtHelper.isTransactionSynchronizationActive()) {
                TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                    @Override
                    public void afterCommitted() {
                        _hmset(groupKey, key, value, seconds);
                    }
                });
            } else {
                _hmset(groupKey, key, value, seconds);
            }
        } catch (Exception e) {
            logger.error("RedisClusterUtil.hmset failed.", e);
            InterfaceMonitor.getInstance().addFail("RedisUtil.hmset.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER);
            throw e;
        }
    }

    private static void _hmset(final String groupKey, final Object key, final Object value, final int seconds) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        try {
            Map<String, String> map = new HashMap<String, String>();

            Object obj = null;
            if (value instanceof Map) {
                for (Entry<String, Object> entry : ((Map<String, Object>) value).entrySet()) {
                    map.put(entry.getKey(), JsonMapper.toJson(entry.getValue()));
                }
            } else {
                List<Field> fieldList = new ArrayList<>();
                fieldList.addAll(Arrays.asList(value.getClass().getDeclaredFields()));
                fieldList.addAll(Arrays.asList(value.getClass().getSuperclass().getDeclaredFields()));
                for (Field field : fieldList) {
                    if ("serialVersionUID".equals(field.getName())) {
                        continue;
                    }
                    if (Modifier.isStatic(field.getModifiers())) {
                        continue;
                    }

                    obj = Reflections.invokeGetter(value, field.getName());
                    if (obj == null) {
                        continue;
                    }

                    if (obj instanceof String && "".equals(obj)) {
                        continue;
                    }
                    map.put(field.getName(), JsonMapper.toJson(obj));
                }
            }
            getRedisClient(groupKey, key).hmset(groupKey + key, map);
            if (seconds > -1) {
                getRedisClient(groupKey, key).expire(groupKey + key, seconds);
            }
            LoggerUtil.logSlow(start, 100, "RedisClusterUtil._hmset hmset");
        } catch (Exception e) {
            logger.error("RedisClusterUtil.hmset failed. redisKey = " + groupKey + key, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.hmset.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
    }


    /**
     * 通过hash取得clazz类型的对象
     *
     * @param groupKey
     * @param key
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T hmget(final String groupKey, Object key, Class<T> clazz) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        T result = null;
        String redisKey = String.format("%s%s", groupKey, key);
        try {
            result = clazz.newInstance();
            Map<String, String> map = getRedisClient(groupKey, key).hgetAll(redisKey);
//			Map<String, String> map = _hgetAll(redisKey);
            LoggerUtil.logSlow(start, 200, "RedisClusterUtil.hget hgetAll");
            if (map == null || map.size() <= 0) {
                return null;
            }
            Field field;
            Object value;
            for (Entry<String, String> entry : map.entrySet()) {
                if (result instanceof Map) {
                    ((Map) result).put(entry.getKey(), entry.getValue());
                } else {
                    try {
                        field = result.getClass().getDeclaredField(entry.getKey());
                    } catch (NoSuchFieldException e) {
                        try {
                            field = result.getClass().getSuperclass().getDeclaredField(entry.getKey());
                        } catch (Exception e1) {
                            continue;
                        }

                    }
                    if (field == null) {
                        continue;
                    }

                    if (field.getType().isAssignableFrom(List.class)) {
                        value = JsonMapper.json2List(entry.getValue(), field.getGenericType().getClass());
                    } else {
                        value = JsonMapper.json2Bean(entry.getValue(), field.getType());
                    }

                    //映射值 result中对应的field字段调用setter
                    Reflections.invokeSetter(result, entry.getKey(), value);
                }
            }

        } catch (Exception e) {
            logger.error("RedisClusterUtil.hget failed. key = " + key
                    + " clazz = " + clazz.getName(), e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.hmget.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return result;
    }

//	/**
//	 * 通过hash取得clazz类型的对象
//	 * @param groupKey
//	 * @param clazz
//	 * @param <T>
//	 * @return
//	 * @since 4.3
//	 * @throws Exception
//	 */
//	@SuppressWarnings("unchecked")
//	public static <T> List<T> mhmget(final String groupKey, String[] keys, Class<T> clazz) throws Exception {
//		List<T> resultList = new ArrayList<T>();
//		try {
//			T result = null;
//			long start = System.currentTimeMillis();
//			List<Map<String, String>> list = _executeBatchCommand(groupKey, keys, new RedisCallable<Map<String, String>>() {
//
//				@Override
//				public Map<String, String> call(String redisKey)
//						throws Exception {
//					return getRedisClient(groupKey, "").hgetAll(redisKey);
////					return _hgetAll(redisKey);
//				}
//
//			});
//			LoggerUtil.logSlow(start, 200, "RedisClusterUtil.mhget " + groupKey);
//
////			Map<String, String> map;
//			for (Map<String, String> map : list) {
//				if (map == null || map.size() <= 0) {
//					return null;
//				}
//				result = clazz.newInstance();
//				Field field = null;
//				Object value;
//				for (Entry<String, String> entry : map.entrySet()) {
//					if (result instanceof Map) {
//						((Map)result).put(entry.getKey(), entry.getValue());
//					}else {
//						try{
//							field = result.getClass().getDeclaredField(entry.getKey());
//						} catch (NoSuchFieldException e){
//							try{
//								field = result.getClass().getSuperclass().getDeclaredField(entry.getKey());
//							} catch (NoSuchFieldException e1){
//								//nothing
//							}
//						}
//						if (field == null) {
//							continue;
//						}
//
//						if (field.getType().isAssignableFrom(List.class)) {
//							value = JsonMapper.json2List(entry.getValue(), field.getGenericType().getClass());
//						}else{
//							value = JsonMapper.json2Bean(entry.getValue(), field.getType());
//						}
//
//						//映射值 result中对应的field字段调用setter
//						Reflections.invokeSetter(result, entry.getKey(), value);
//					}
//				}
//				resultList.add(result);
//			}
//
//		} catch (Exception e) {
//			logger.error("RedisClusterUtil.hget failed. groupKey:{} keys:{} clazz:{}", groupKey, keys, clazz.getName(), e);
//			throw e;
//		}
//		return resultList;
//	}
//
//	/**
//	 * 批量执行hmset
//	 *
//	 * @param groupKey
//	 * @param kvMap
//	 * @param seconds
//	 * @since 4.3
//	 */
//	public static void mhmset(final String groupKey, final Map<String, Object> kvMap, final int seconds) {
//		for (Entry<String, Object> entry : kvMap.entrySet()) {
//			final String key = entry.getKey();
//			final Object value = entry.getValue();
//			ExecutorHelper.getInstance().execute("REDIS_MULTI", new Callable<String>() {
//				@Override
//				public String call() throws Exception {
//					_hmset(groupKey, key, value, seconds);
//					return null;
//				}
//			});
//		}
//	}

    /**
     * hash的field个数查询
     *
     * @param groupKey
     * @param key
     * @return
     */
    public static long hlen(final String groupKey, final Object key) {
        if (key == null) {
            return 0;
        }
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        long result = 0;
        String redisKey = String.format("%s%s", groupKey, key);
        try {
            result = getRedisClient(groupKey, key).hlen(redisKey);
        } catch (Exception e) {
            logger.error("RedisClusterUtil.hlen failed. key = " + redisKey, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.hlen.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return result;
    }

    /**
     * 通过field取得
     *
     * @param groupKey
     * @param key
     * @param field
     * @return
     */
    public static <T> T hget(final String groupKey, final Object key, final String field, final Class<T> clazz) {
        final String redisKey = String.format("%s%s", groupKey, key);
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        try {
            String result = getRedisClient(groupKey, key).hget(redisKey, field);
            LoggerUtil.logSlow(start, 200, "RedisClusterUtil.hget " + redisKey);
            return JsonMapper.json2Bean(result, clazz);
        } catch (Exception e) {
            logger.error("RedisClusterUtil.hget failed. key = " + key + " field = " + field, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.hget.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return null;
    }


    /**
     * redis StringKey 自增1
     *
     * @param groupKey
     * @param key
     * @return
     */
    public static Long incr(final String groupKey, final Object key, final int seconds) {
        final String redisKey = String.format("%s%s", groupKey, key);
        Callable<Long> getCallable = getLongCallable(groupKey, key);
        Callable<Long> incrCallable = new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                Long result = 0L;
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    result = getRedisClient(groupKey, key).incr(redisKey);
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(redisKey, seconds);
                    }
                    LoggerUtil.logSlow(start, 200, "RedisClusterUtil.incr " + redisKey);
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.incr failed. redisKey = " + redisKey, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.incr.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
                return result;
            }
        };
        return TransactionExtHelper.incr(redisKey, 0L, 1L, getCallable, incrCallable);
    }


    /**
     * redis StringKey 自减1
     *
     * @param groupKey
     * @param key
     * @return
     */
    public static Long decr(final String groupKey, final Object key, final int seconds) {
        final String redisKey = String.format("%s%s", groupKey, key);
        Callable<Long> getCallable = getLongCallable(groupKey, key);
        Callable<Long> decrCallable = new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                Long result = 0L;
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    result = getRedisClient(groupKey, key).decr(redisKey);
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(redisKey, seconds);
                    }
                    LoggerUtil.logSlow(start, 200, "RedisClusterUtil.decr " + redisKey);
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.decr failed. redisKey = " + redisKey, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.decr.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
                return result;
            }
        };
        return TransactionExtHelper.decr(redisKey, 0L, 1L, getCallable, decrCallable);
    }

    private static Callable<Long> getLongCallable(final String groupKey, final Object key) {
        return new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                String redisKey = String.format("%s%s", groupKey, key);
                Long result = 0L;
                try {
                    String value = getRedisClient(groupKey, key).get(redisKey);
                    if (null == value) {
                        return null;
                    }
                    result = Long.parseLong(value);
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.incr failed. redisKey = " + redisKey, e);
                    throw e;
                }
                return result;
            }
        };
    }


    /**
     * redis StringKey 自增
     *
     * @param groupKey
     * @param key
     * @param incyNum  增数
     * @return
     */
    public static Long incrby(final String groupKey, final Object key, final Object incyNum, final int seconds) {
        final String redisKey = String.format("%s%s", groupKey, key);
        Callable<Long> getCallable = getLongCallable(groupKey, key);
        Callable<Long> decrCallable = new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                Long result = 0L;
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    result = getRedisClient(groupKey, key).incrBy(redisKey, Long.parseLong(incyNum.toString()));
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(redisKey, seconds);
                    }
                    LoggerUtil.logSlow(start, 200, "RedisClusterUtil.incrby " + redisKey);
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.incr failed. redisKey = " + redisKey, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.incrby.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
                return result;
            }
        };
        return TransactionExtHelper.incr(redisKey, 0L, Long.parseLong(incyNum.toString()), getCallable, decrCallable);
    }

    private static Callable<Long> hgetLongCallable(final String groupKey, final Object key, final String field) {
        return new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                String redisKey = String.format("%s%s", groupKey, key);
                Long result = 0L;
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    String value = getRedisClient(groupKey, key).hget(redisKey, field);
                    if (null == value) {
                        return null;
                    }
                    result = Long.parseLong(value);
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.hgetLongCallable failed. redisKey = " + redisKey, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.hgetLongCallable.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
                return result;
            }
        };
    }

    private static Callable<Double> hgetDoubleCallable(final String groupKey, final Object key, final String field) {
        return new Callable<Double>() {
            @Override
            public Double call() throws Exception {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                String redisKey = String.format("%s%s", groupKey, key);
                Double result = 0D;
                try {
                    String value = getRedisClient(groupKey, key).hget(redisKey, field);
                    if (null == value) {
                        return null;
                    }
                    result = Double.parseDouble(value);
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.hgetDoubleCallable failed. redisKey = " + redisKey, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.hgetDoubleCallable.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
                return result;
            }
        };
    }

    /**
     * redis  自增
     *
     * @param groupKey
     * @param key
     * @param incyNum  增数
     * @return
     */
    public static void hincrby(final String groupKey, final Object key, final String field, final Object incyNum, final int seconds) {
        hincrby(groupKey, key, field, incyNum, seconds, true);
    }

    /**
     * redis  自增
     *
     * @param groupKey
     * @param key
     * @param incyNum  增数
     * @return
     */
    public static void hincrby(final String groupKey, final Object key, final String field, final Object incyNum, final int seconds, final boolean isTx) {
        final String redisKey = String.format("%s%s", groupKey, key);
        if (TransactionExtHelper.isTransactionSynchronizationActive() && isTx) {
            Callable<Long> getCallable = hgetLongCallable(groupKey, key, field);
            Callable<Long> incrCallable = new Callable<Long>() {
                @Override
                public Long call() throws Exception {
                    return _hincrby(groupKey, key, field, incyNum, seconds);
                }
            };
            TransactionExtHelper.hincr(redisKey, field, 0L, Long.parseLong(incyNum.toString()), getCallable, incrCallable);
        } else {
            _hincrby(groupKey, key, field, incyNum, seconds);
        }
    }

    private static Long _hincrby(final String groupKey, final Object key, final String field, final Object incyNum, final int seconds) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        final String redisKey = String.format("%s%s", groupKey, key);
        try {
            Long result = getRedisClient(groupKey, key).hincrBy(redisKey, field, Long.parseLong(incyNum.toString()));
            if (seconds > -1) {
                getRedisClient(groupKey, key).expire(redisKey, seconds);
            }
            return result;
        } catch (Exception e) {
            logger.error("RedisClusterUtil.hincrby failed. groupKey = " + groupKey
                    + " key = " + key
                    + " field = " + field, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.hincrby.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
    }

    /**
     * redis StringKey 自增
     * @param groupKey
     * @param key
     * @param incyNum 增数
     * @return
     */
    public static void hincrbyFloat(final String groupKey, final Object key, final String field, final Object incyNum, final int seconds) {
        final String redisKey = String.format("%s%s", groupKey, key);
        if (TransactionExtHelper.isTransactionSynchronizationActive()) {
            Callable<Double> getCallable = hgetDoubleCallable(groupKey, key, field);
            Callable<Double> incrCallable = new Callable<Double>() {
                @Override
                public Double call() throws Exception {
                    return _hincrbyFloat(groupKey, key, field, incyNum, seconds);
                }
            };
            TransactionExtHelper.hincr(redisKey, field, 0.0D, Double.parseDouble(incyNum.toString()), getCallable, incrCallable);
        }else {
            _hincrbyFloat(groupKey, key, field, incyNum, seconds);
        }
    }

    private static Double _hincrbyFloat(final String groupKey, final Object key, final String field, final Object incyNum, final int seconds) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        final String redisKey = String.format("%s%s", groupKey, key);
        try {
            Double result = getRedisClient(groupKey, key).hincrByFloat(redisKey.getBytes(), field.getBytes(), Double.parseDouble(incyNum.toString()));
            if (seconds > -1) {
                getRedisClient(groupKey, key).expire(redisKey, seconds);
            }
            return result;
        } catch (Exception e) {
            logger.error("RedisClusterUtil.hincrbyFloat failed. groupKey = " + groupKey
                    + " key = " + key
                    + " field = " + field, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.hincrbyFloat.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
    }

    /**
     * 设置过期时间
     *
     * @param groupKey
     * @param key
     * @param seconds
     */
    public static void expire(final String groupKey, final Object key, final int seconds) {
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                String redisKey = String.format("%s%s", groupKey, key);
                try {
                    getRedisClient(groupKey, key).expire(redisKey, seconds);
                    LoggerUtil.logSlow(start, 50, "RedisClusterUtil.expire " + redisKey);
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.expire failed. redisKey = " + redisKey, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.expire.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };
        if (TransactionExtHelper.isTransactionSynchronizationActive()) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
    }

    /**
     * 设置过期时间
     *
     * @param groupKey
     * @param key
     * @param seconds
     */
    public static Long expireWithRes(final String groupKey, final Object key, final int seconds) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        String redisKey = String.format("%s%s", groupKey, key);
        long value = 0L;
        try {
            value = getRedisClient(groupKey, key).expire(redisKey, seconds);
            LoggerUtil.logSlow(start, 200, "RedisClusterUtil.expireWithRes " + redisKey);
        } catch (Exception e) {
            logger.error("RedisClusterUtil.expireWithRes failed. redisKey = " + redisKey, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.expireWithRes.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return value;
    }

    public static void expireAt(final String groupKey, final Object key, final long seconds) {

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                String redisKey = String.format("%s%s", groupKey, key);
                try {
                    getRedisClient(groupKey, key).expireAt(redisKey, seconds);
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.expireAt failed. redisKey = " + redisKey, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.expireAt.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };
        if (TransactionExtHelper.isTransactionSynchronizationActive()) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
    }

    public static void delDirect(final String groupKey, final Object key) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        try {
            String redisKey = String.format("%s%s", groupKey, key);
            getRedisClient(groupKey, key).del(redisKey);
            LoggerUtil.logSlow(start, 100, "RedisClusterUtil.del " + redisKey);
        } catch (Exception e) {
            logger.error("RedisClusterUtil.del failed. groupkey = " + groupKey + "key = " + JsonMapper.toJson(key), e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.del.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
    }

    public static void del(final String groupKey, final Object key) {
        final String redisKey = String.format("%s%s", groupKey, key);
        TransactionExtHelper.deleteLocalTxCache(redisKey, new Runnable() {
            @Override
            public void run() {
                delDirect(groupKey, key);
            }
        });
    }

    public static void del(final String groupKey, final Object key,final boolean isTx) {
        final String redisKey = String.format("%s%s", groupKey, key);
        try {
            if (isTx && TransactionExtHelper.isTransactionSynchronizationActive()) {
                TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                    @Override
                    public void afterCommitted() {
                        del(groupKey, key);
                    }
                });
            } else {
                delDirect(groupKey, key);
            }
        } catch (Exception e) {
            logger.error("RedisClusterUtil.del failed.", e);
            throw e;
        }
    }



    public static boolean exists(final String groupKey, final Object key) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        String redisKey = String.format("%s%s", groupKey, key);
        boolean result = false;
        try {
            result = getRedisClient(groupKey, key).exists(redisKey);
            LoggerUtil.logSlow(start, 100, "RedisClusterUtil.exists " + redisKey);
        } catch (Exception e) {
            logger.error("RedisClusterUtil.exists failed. groupkey = {} key = {}", groupKey, key, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.exists.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return result;
    }

    public static void hdel(final String groupKey, final Object key, final String... keys) {
        final String redisKey = String.format("%s%s", groupKey, key);
        TransactionExtHelper.hdeleteLocalTxCache(redisKey, keys, new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    getRedisClient(groupKey, key).hdel(redisKey, keys);
                    LoggerUtil.logSlow(start, 200, "RedisClusterUtil.hdel " + redisKey);
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.hdel failed. groupkey = " + groupKey + "keys = " + keys, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.hdel.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        });
    }

//	public static Set<?> hkeys(final String groupKey, final Object key){
//        final String redisKey = String.format("%s%s", groupKey, key);
//        String[] keys = TransactionExtHelper.hKeysFromLocalTxCache(redisKey, new Callable<String[]>() {
//            @Override
//            public String[] call() throws Exception {
//                try {
//                	long start = System.currentTimeMillis();
//                    Set<String> hkeys = getRedisClient(groupKey, key).hkeys(redisKey);
//                    LoggerUtil.logSlow(start, 200, "RedisClusterUtil.hkeys " + redisKey);
//                    return hkeys.toArray(new String[hkeys.size()]);
//                } catch(Exception e){
//                    logger.error("RedisClusterUtil.hkeys failed. groupkey = " + groupKey + "key = " + key, e);
//                }
//                return null;
//            }
//        });
//        return new TreeSet<String>(Arrays.asList(keys));
//	}

    public static void sadd(String groupKey, Object key, int seconds, String... values) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        try {
            getRedisClient(groupKey, key).sadd(groupKey + key, values);
            if (seconds > -1) {
                getRedisClient(groupKey, key).expire(groupKey + key, seconds);
            }
        } catch (Exception e) {
            logger.error("RedisClusterUtil.sadd failed. groupKey:{}, key:{}, values:{}", groupKey, key, values, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.sadd.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
    }

    public static long scard(String groupKey, Object key) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        try {
            return getRedisClient(groupKey, key).scard(groupKey + key);
        } catch (Exception e) {
            logger.error("RedisClusterUtil.scard failed. groupKey:{}, key:{}", groupKey, key, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.scard.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
    }

    public static Set<String> smembers(String groupKey, Object key) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        try {
            return getRedisClient(groupKey, key).smembers(groupKey + key);
        } catch (Exception e) {
            logger.error("RedisClusterUtil.smembers failed. groupKey:{}, key:{}", groupKey, key, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.smembers.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
    }

    public static Boolean sismembers(String groupKey, Object key, String menber) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        try {
            return getRedisClient(groupKey, key).sismember(groupKey + key, menber);
        } catch (Exception e) {
            logger.error("RedisClusterUtil.sismember failed. groupKey:{}, key:{} menber:{}", groupKey, key, menber, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.sismember.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
    }

    public static void srem(String groupKey, Object key, String... value) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        try {
            getRedisClient(groupKey, key).srem(groupKey + key, value);
        } catch (Exception e) {
            logger.error("RedisClusterUtil.srem failed. groupKey:{}, key:{} values:{}", groupKey, key, value, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.srem.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
    }


    public static void rpushList(final String groupKey, final Object key, final List value, final int seconds) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    String[] array = new String[value.size()];
                    for (int i = 0; i < value.size(); i++) {
                        array[i] = JsonMapper.toJson(value.get(i));
                    }
                    getRedisClient(groupKey, key).rpush(groupKey + key, array);
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(groupKey + key, seconds);
                    }
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.lpushList failed. redisKey = " + groupKey + key, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.rpushList.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };
        if (TransactionExtHelper.isTransactionSynchronizationActive()) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
    }


    public static <T> void trylpush(final String groupKey, final Object key, Class<T> TClazz, final Object value, final int seconds) {
        if (lrange(groupKey, key, TClazz, 0, -1) != null) {
            lpush(groupKey, key, value, seconds);
        }
    }

    public static void lpush(final String groupKey, final Object key, final Object value, final int seconds) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    if (value instanceof String) {
                        getRedisClient(groupKey, key).lpush(groupKey + key, (String) value);
                    } else {
                        getRedisClient(groupKey, key).lpush(groupKey + key, JsonMapper.toJson(value));
                    }
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(groupKey + key, seconds);
                    }
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.lpush failed. redisKey = " + groupKey + key, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.lpush.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };
        if (TransactionExtHelper.isTransactionSynchronizationActive()) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
    }

    public static void rpush(final String groupKey, final Object key, final Object value, final int seconds) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    if (value instanceof String) {
                        getRedisClient(groupKey, key).rpush(groupKey + key, (String) value);
                    } else {
                        getRedisClient(groupKey, key).rpush(groupKey + key, JsonMapper.toJson(value));
                    }
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(groupKey + key, seconds);
                    }
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.rpush failed. redisKey = " + groupKey + key, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.rpush.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };
        if (TransactionExtHelper.isTransactionSynchronizationActive()) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
    }

    public static void lpush(final String groupKey, final Object key, final Object value, final int seconds, boolean isTx) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    if (value instanceof String) {
                        getRedisClient(groupKey, key).lpush(groupKey + key, (String) value);
                    } else {
                        getRedisClient(groupKey, key).lpush(groupKey + key, JsonMapper.toJson(value));
                    }
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(groupKey + key, seconds);
                    }
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.lpush failed. redisKey = " + groupKey + key, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.lpush.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };
        if (isTx) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
    }


    public static void rpushAndTrim(final String groupKey, final Object key, final int len, final int seconds, final String... value) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    getRedisClient(groupKey, key).rpush(groupKey + key, value);
                    getRedisClient(groupKey, key).ltrim(groupKey + key, len * -1, -1);
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(groupKey + key, seconds);
                    }
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.rpushAndTrim failed. redisKey = " + groupKey + key, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.rpushAndTrim.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };
        if (TransactionExtHelper.isTransactionSynchronizationActive()) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
    }

    public static void rpush(final String groupKey, final Object key, final int seconds, final String... value) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    getRedisClient(groupKey, key).rpush(groupKey + key, value);
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(groupKey + key, seconds);
                    }
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.rpush failed. redisKey = " + groupKey + key, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.rpush.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };
        if (TransactionExtHelper.isTransactionSynchronizationActive()) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
    }


    public static void lpushAndTrim(final String groupKey, final Object key, final Object value, final int len, final int seconds) {
        lpushAndTrim(groupKey, key, len, seconds, JsonMapper.toJson(value));
    }

    public static void lpushAndTrim(final String groupKey, final Object key, final int len, final int seconds, final String... value) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    getRedisClient(groupKey, key).lpush(groupKey + key, value);
                    getRedisClient(groupKey, key).ltrim(groupKey + key, 0, len - 1);
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(groupKey + key, seconds);
                    }
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.lpushAndTrim failed. redisKey = " + groupKey + key, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.lpushAndTrim.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };
        if (TransactionExtHelper.isTransactionSynchronizationActive()) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
    }

    /**
     * 通过TClazz取得List<TClazz>
     *
     * @param groupKey
     * @param key
     * @param TClazz
     * @return
     */
    public static <T> List<T> lrange(final String groupKey, Object key, Class<T> TClazz, int begin, int end) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        List<T> result = null;
        String redisKey = String.format("%s%s", groupKey, key);
        try {
            List<String> list = getRedisClient(groupKey, key).lrange(redisKey, begin, end);
            if (list == null || list.size() <= 0) {
                return null;
            }
            result = new ArrayList<>();
            for (String s : list) {
//				if("String".equals(TClazz.getSimpleName())) {
//					result.add((T) s);
//				} else
                if (s.startsWith("\"{")) {
                    // 这种场合： "{\"long_address\":\"北京市朝阳区望京西园429号里外里公寓底商\",\"short_address\":\"伯特丽西饼屋(望京店)\",\"longitude\":\"116.477814\",\"latitude\":\"39.998660\"}"
                    result.add(JsonMapper.json2Bean(s.replace("\\", "").replace("\"{", "{").replace("}\"", "}"), TClazz));
                } else {
                    try {
                        result.add(JsonMapper.json2Bean(s, TClazz));
                    } catch (Exception e) {
                        if ("String".equals(TClazz.getSimpleName())) {
                            result.add((T) s);
                        } else {
                            throw e;
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("RedisClusterUtil.lrange failed. key = " + key
                    + " clazz = " + TClazz.getName(), e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.lrange.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return result;
    }

    /**
     * list长度
     *
     * @param groupKey
     * @param key
     * @return
     */
    public static long llen(final String groupKey, final Object key) {
        if (key == null) {
            return 0;
        }
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        long result = 0;
        String redisKey = String.format("%s%s", groupKey, key);
        try {
            result = getRedisClient(groupKey, key).llen(redisKey);
        } catch (Exception e) {
            logger.error("RedisClusterUtil.llen failed. key = " + redisKey, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.llen.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return result;
    }


    public static void lrem(final String groupKey, final Object key, final int count, final Object value, final int seconds) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }
//		try{
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    if (value instanceof String) {
                        getRedisClient(groupKey, key).lrem(groupKey + key, count, String.valueOf(value));
                        getRedisClient(groupKey, key).lrem(groupKey + key, count, JsonMapper.toJson(value));
                    } else {
                        getRedisClient(groupKey, key).lrem(groupKey + key, count, JsonMapper.toJson(value));
                    }
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(groupKey + key, seconds);
                    }

                } catch (Exception e) {
                    logger.error("RedisClusterUtil.lrem failed. redisKey = " + groupKey + key, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.lrem.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };
        if (TransactionExtHelper.isTransactionSynchronizationActive()) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
//		} catch (Exception e){
//			logger.error("lrem failed.", e);
//		}
    }


    public static void zadd(final String groupKey, final Object key, final long score, final Object value, final int seconds) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    if (value instanceof String) {
                        getRedisClient(groupKey, key).zadd(groupKey + key, score, String.valueOf(value));
                    } else {
                        getRedisClient(groupKey, key).zadd(groupKey + key, score, JsonMapper.toJson(value));
                    }
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(groupKey + key, seconds);
                    }
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.zadd failed. redisKey = " + groupKey + key, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.zadd.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };
        if (TransactionExtHelper.isTransactionSynchronizationActive()) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
    }

    public static void zadd(final String groupKey, final Object key, final long score, final Object value, final int seconds, boolean isTx) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    if (value instanceof String) {
                        getRedisClient(groupKey, key).zadd(groupKey + key, score, String.valueOf(value));
                    } else {
                        getRedisClient(groupKey, key).zadd(groupKey + key, score, JsonMapper.toJson(value));
                    }
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(groupKey + key, seconds);
                    }
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.zadd failed. redisKey = " + groupKey + key, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.zadd.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };
        if (isTx) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
    }

    public static <T> List<T> zrevrange(String groupKey, Object key, Class<T> TClazz, int start, int stop) {
        long startTime = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        List<T> result = null;
        String redisKey = String.format("%s%s", groupKey, key);
        try {
            Set<String> set = getRedisClient(groupKey, key).zrevrange(redisKey, start, stop);
            if (set == null || set.size() <= 0) {
                return null;
            }
            result = new ArrayList<>();
            for (String s : set) {
                if ("String".equals(TClazz.getSimpleName())) {
                    result.add((T) s);
                } else if (s.startsWith("\"{")) {
                    result.add(JsonMapper.json2Bean(s.replace("\\", "").replace("\"{", "{").replace("}\"", "}"), TClazz));
                } else {
                    result.add(JsonMapper.json2Bean(s, TClazz));
                }
            }
        } catch (Exception e) {
            logger.error("RedisClusterUtil.zrevrange failed. key = " + key
                    + " clazz = " + TClazz.getName(), e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.zrevrange.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - startTime, executeResult);
        }
        return result;
    }


    public static <T> List<T> zrange(String groupKey, Object key, Class<T> TClazz, int start, int stop) {
        long startTime = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        List<T> result = null;
        String redisKey = String.format("%s%s", groupKey, key);
        try {
            InterfaceMonitor.getInstance().addTotal("RedisUtil.zrange.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER);
            Set<String> set = getRedisClient(groupKey, key).zrange(redisKey, start, stop);
            if (set == null || set.size() <= 0) {
                return null;
            }
            result = new ArrayList<>();
            for (String s : set) {
                if ("String".equals(TClazz.getSimpleName())) {
                    result.add((T) s);
                } else if (s.startsWith("\"{")) {
                    result.add(JsonMapper.json2Bean(s.replace("\\", "").replace("\"{", "{").replace("}\"", "}"), TClazz));
                } else {
                    result.add(JsonMapper.json2Bean(s, TClazz));
                }
            }
        } catch (Exception e) {
            logger.error("RedisClusterUtil.zrevrange failed. key = " + key
                    + " clazz = " + TClazz.getName(), e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.zrange.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - startTime, executeResult);
        }
        return result;
    }


    public static void zrem(final String groupKey, final Object key, final Object value, final int seconds) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }
//		try{
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    if (value instanceof String) {
                        getRedisClient(groupKey, key).zrem(groupKey + key, String.valueOf(value));
                    } else {
                        getRedisClient(groupKey, key).zrem(groupKey + key, JsonMapper.toJson(value));
                    }
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(groupKey + key, seconds);
                    }
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.zrem failed. redisKey = " + groupKey + key, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.zrem.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };
        if (TransactionExtHelper.isTransactionSynchronizationActive()) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
//		} catch (Exception e){
//			logger.error("zrem failed.", e);
//		}
    }

    public static void zrem(final String groupKey, final Object key, final Object value, final int seconds, boolean isTx) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || value == null) {
            return;
        }
//		try{
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                try {
                    InterfaceMonitor.getInstance().addTotal("RedisUtil.zrem.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER);
                    if (value instanceof String) {
                        getRedisClient(groupKey, key).zrem(groupKey + key, String.valueOf(value));
                    } else {
                        getRedisClient(groupKey, key).zrem(groupKey + key, JsonMapper.toJson(value));
                    }
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(groupKey + key, seconds);
                    }
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.zrem failed. redisKey = " + groupKey + key, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.zrem.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };
        if (isTx) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
//		} catch (Exception e){
//			logger.error("zrem failed.", e);
//		}
    }

    public static int zcount(final String groupKey, final Object key, String min, String max) {
        if (key == null) {
            return 0;
        }
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        Long result = null;
        String redisKey = String.format("%s%s", groupKey, key);
        try {
            result = getRedisClient(groupKey, key).zcount(groupKey + key, Double.parseDouble(min), Double.parseDouble(max));
        } catch (Exception e) {
            logger.error("RedisClusterUtil.zcount failed. key = " + redisKey, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.zcount.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return result == null ? 0 : result.intValue();
    }


    public static int zcard(String groupKey, Object key) {
        if (key == null) {
            return 0;
        }
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        Long result = null;
        String redisKey = String.format("%s%s", groupKey, key);
        try {
            result = getRedisClient(groupKey, key).zcard(groupKey + key);
        } catch (Exception e) {
            logger.error("RedisClusterUtil.llen failed. key = " + redisKey, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.zcard.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return result == null ? 0 : result.intValue();
    }

    public static int setnx(String groupKey, Object key, Object value, int seconds) {
        if (key == null) {
            return 0;
        }
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        Long result = null;
        String redisKey = String.format("%s%s", groupKey, key);
        try {
            result = getRedisClient(groupKey, key).setnx(redisKey, JsonMapper.toJson(value));
            logger.debug("RedisClusterUtil.setnx reslut: {}{} {}", groupKey, key, result);
            if (result != null && result == 1) {
                getRedisClient(groupKey, key).expire(redisKey, seconds == -1 ? 2 : seconds); //2s
            }
        } catch (Exception e) {
            logger.error("RedisClusterUtil.setnx failed. key = " + redisKey, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.setnx.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return result == null ? 0 : result.intValue();
    }

//	//由于hgetall性能再超过50个field的时候很差，用此方法
//	private static Map<String, String> _hgetAll(String redisKey){
//
//		Map<String, String> result = new HashMap<String, String>();
//		Set<String> hkeys = getRedisClient(redisKey, "").hkeys(redisKey);
//		if (hkeys == null) {
//			return result;
//		}
//
//		//批量获取
//		Pipeline pipeline = getRedisClient(redisKey, "").pipelined();
//		if (pipeline == null) {
//			return result;
//		}
//		Map<String, Response<String>> kv = new HashMap<String, Response<String>>();
//		for (String field : hkeys) {
//			kv.put(field, pipeline.hget(redisKey, field));
//		}
//		pipeline.sync();
//
//		//组装map
//		for (String field : hkeys) {
//			result.put(field, kv.get(field).get());
//		}
//		return result;
//	}

    public static <T> List<T> hvals(final String groupKey, Object key, Class<T> clazz) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        List<T> resultList = new ArrayList<>();
        String redisKey = String.format("%s%s", groupKey, key);
        try {
            List<String> list = getRedisClient(groupKey, key).hvals(redisKey);
            for (String entry : list) {
                resultList.add(JsonMapper.json2Bean(entry, clazz));
            }
        } catch (Exception e) {
            logger.error("RedisClusterUtil.hvals failed. groupKey:{},key:{},clazz:{}", groupKey, key, clazz.getName(), e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.hvals.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return resultList;
    }

    public static Set<String> hkeys(final String groupKey, final Object key) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        final String redisKey = String.format("%s%s", groupKey, key);
        try {
            Set<String> result = getRedisClient(groupKey, key).hkeys(redisKey);
            LoggerUtil.logSlow(start, 100, "RedisClusterUtil.hkeys " + redisKey);
            return result;
        } catch (Exception e) {
            logger.error("RedisClusterUtil.hkeys failed. key = " + key , e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.hkeys.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return null;
    }


    public static <T> T getSet(String groupKey, Object key, Object value, Class<T> clazz, int seconds) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        String redisKey = String.format("%s%s", groupKey, key);
        try {
            T result = JsonMapper.json2Bean(getRedisClient(groupKey, key).getSet(redisKey, JsonMapper.toJson(value)), clazz);
            if (result != null) {
                getRedisClient(groupKey, key).expire(redisKey, seconds == -1 ? 2 : seconds);
            }
            return result;
        } catch (Exception e) {
            logger.error("RedisClusterUtil.getSet failed. groupKey:{}, key:{}, clazz:{}", groupKey, key, clazz.getName(), e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.getSet.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
    }

    /**
     * redis StringKey 自增
     *
     * @param groupKey
     * @param key
     * @param incyNum  增数
     * @return
     */
    public static Double incrbyFloat(final String groupKey, Object key, Object incyNum) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        String redisKey = String.format("%s%s", groupKey, key);
        Double result = 0.0;
        try {
            result = getRedisClient(groupKey, key).incrByFloat(redisKey, Double.parseDouble(incyNum.toString()));
        } catch (Exception e) {
            logger.error("RedisClusterUtil.incrByFloat failed. redisKey = " + redisKey, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.incrByFloat.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return result;
    }

    public static void zincrby(final String groupKey, final Object key, final double score, final Object member, final int seconds, boolean isTx) {
        if (key == null || StringUtils.isEmpty(String.valueOf(key)) || member == null) {
            return;
        }
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                String executeResult = InterfaceMonitor.ERROR_SUCC;
                String redisKey = String.format("%s%s", groupKey, key);
                try {
                    if (member instanceof String) {
                        getRedisClient(groupKey, key).zincrby(redisKey, score, String.valueOf(member));
                    } else {
                        getRedisClient(groupKey, key).zincrby(redisKey, score, JsonMapper.toJson(member));
                    }
                    if (seconds > -1) {
                        getRedisClient(groupKey, key).expire(redisKey, seconds);
                    }
                } catch (Exception e) {
                    logger.error("RedisClusterUtil.zincrby failed. redisKey = " + redisKey, e);
                    executeResult = InterfaceMonitor.ERROR_FAIL;
                    throw e;
                } finally {
                    InterfaceMonitor.getInstance().setDuration("RedisUtil.zincrby.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
                }
            }
        };
        if (isTx) {
            TransactionExtHelper.addCommittedCallback(new ITxCommittedCallback() {
                @Override
                public void afterCommitted() {
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
    }

    public static List<Tuple> zrevrangeWithScores(String groupKey, Object key, int start, int stop) {
        long startTime = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        List<Tuple> result = null;
        String redisKey = String.format("%s%s", groupKey, key);
        try {
            Set<Tuple> set = RedisClusterUtil.getRedisClient(groupKey, key).zrevrangeWithScores(redisKey, start, stop);
            if (set == null || set.size() <= 0) {
                return null;
            }
            result = new ArrayList<>();
            for (Tuple tuple : set) {
                result.add(tuple);
            }
        } catch (Exception e) {
            logger.error("RedisClusterUtil.zrevrangeWithScores failed. redisKey:{} start:{} stop:{}", redisKey, start, stop, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.zrevrangeWithScores.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - startTime, executeResult);
        }
        return result;
    }

    public static List<Tuple> zrangeWithScores(String groupKey, Object key, int start, int stop) {
        long startTime = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        List<Tuple> result = null;
        String redisKey = String.format("%s%s", groupKey, key);
        try {
            Set<Tuple> set = RedisClusterUtil.getRedisClient(groupKey, key).zrangeWithScores(redisKey, start, stop);
            if (set == null || set.size() <= 0) {
                return null;
            }
            result = new ArrayList<>();
            for (Tuple tuple : set) {
                result.add(tuple);
            }
        } catch (Exception e) {
            logger.error("RedisClusterUtil.zrangeWithScores failed. redisKey:{} start:{} stop:{}", redisKey, start, stop, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.zrangeWithScores.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - startTime, executeResult);
        }
        return result;
    }

    public static Double zscore(String groupKey, String key, Object member) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        Double result = null;
        String redisKey = String.format("%s%s", groupKey, key);
        try {
            if (member instanceof String) {
                result = getRedisClient(groupKey, key).zscore(redisKey, String.valueOf(member));
            } else {
                result = getRedisClient(groupKey, key).zscore(redisKey, JsonMapper.toJson(member));
            }
        } catch (Exception e) {
            logger.error("RedisClusterUtil.zscore failed. redisKey = " + redisKey, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.zscore.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return result;
    }

    public static Long zrank(String groupKey, String key, Object member) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        Long result = null;
        String redisKey = String.format("%s%s", groupKey, key);
        try {
            if (member instanceof String) {
                result = getRedisClient(groupKey, key).zrank(redisKey, String.valueOf(member));
            } else {
                result = getRedisClient(groupKey, key).zrank(redisKey, JsonMapper.toJson(member));
            }
        } catch (Exception e) {
            logger.error("RedisClusterUtil.zrank failed. redisKey = " + redisKey, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.zrank.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return result;
    }

    public static Long zrevrank(String groupKey, String key, Object member) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        Long result;
        String redisKey = String.format("%s%s", groupKey, key);
        try {
            if (member instanceof String) {
                result = getRedisClient(groupKey, key).zrevrank(redisKey, String.valueOf(member));
            } else {
                result = getRedisClient(groupKey, key).zrevrank(redisKey, JsonMapper.toJson(member));
            }
        } catch (Exception e) {
            logger.error("RedisClusterUtil.zrevrank failed. redisKey = " + redisKey, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.zrevrank.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return result;
    }

    public static ScanResult<String> sscan(String groupKey, String key, String cursor) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        String redisKey = String.format("%s%s", new Object[]{groupKey, key});
        ScanResult<String> result;
        try {
            result = getRedisClient(groupKey, key).sscan(redisKey, cursor);
        } catch (Exception e) {
            logger.error("RedisClusterUtil.sscan failed. redisKey = " + redisKey, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.sscan.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }

        return result;
    }

    public static ScanResult<byte[]> sscan(String groupKey, String key, String cursor, ScanParams scanParams) {
        long start = System.currentTimeMillis();
        String executeResult = InterfaceMonitor.ERROR_SUCC;
        String redisKey = String.format("%s%s", new Object[]{groupKey, key});
        ScanResult<byte[]> result;
        try {
            result = getRedisClient(groupKey, key).sscan(redisKey.getBytes(), cursor.getBytes(), scanParams);
        } catch (Exception e) {
            logger.error("RedisClusterUtil.sscan failed. redisKey:{} cursor:{} scanParams:{} ", redisKey, cursor, scanParams, e);
            executeResult = InterfaceMonitor.ERROR_FAIL;
            throw e;
        } finally {
            InterfaceMonitor.getInstance().setDuration("RedisUtil.sscan.groupKey:" + groupKey, InterfaceMonitor.TYPE_REDIS_CLUSTER, System.currentTimeMillis() - start, executeResult);
        }
        return result;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
