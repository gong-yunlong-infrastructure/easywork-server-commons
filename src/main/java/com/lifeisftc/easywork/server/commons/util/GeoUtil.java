package com.lifeisftc.easywork.server.commons.util;

import static java.lang.Math.toRadians;

/**
 * 经纬度计算相关
 * @author gongyunlong
 */
public class GeoUtil {
    private static final int EARTH_RADIUS = 6378137;

    private static double rad(double angle) {
        return angle * Math.PI / 180.0;
    }

    /**
     * 计算地球球面距离
     * @param sLon 起点经度
     * @param sLat 起点纬度
     * @param eLon 终点经度
     * @param eLat 终点纬度
     * @return 距离/米
     */
    public static int getEarthDistance(double sLon, double sLat, double eLon, double eLat) {
        // 经度差值
        double dx = sLon - eLon;
        // 纬度差值
        double dy = sLat - eLat;
        // 平均纬度
        double b = (sLat + eLat) / 2.0;
//        double Lx = dx / 180.0 * Math.PI * EARTH_RADIUS * Math.cos(b / 180.0 * Math.PI); // 东西距离
        // 东西距离
        double lx = dx * 111319.4908 * Math.cos(b * 0.01745329252);
        // 南北距离
        double ly = EARTH_RADIUS * toRadians(dy);
        int res = (int) Math.sqrt(lx * lx + ly * ly);
        // 大于500公里时，新的距离算法和老版本的距离算法相差100m左右
        if (res > 400000) {
            return getEarthDistanceOrgi(sLon, sLat, eLon, eLat);
        }
        // 用平面的矩形对角距离公式计算总距离
        return res;
    }

    /**
     * 计算两点间球面距离
     * @param sLon 起点经度
     * @param sLat 起点纬度
     * @param eLon 终点经度
     * @param eLat 终点纬度
     * @return 距离/米
     */
    public static int getEarthDistanceOrgi(double sLon, double sLat, double eLon, double eLat) {
        double red_lat1 = rad(sLat);
        double red_lat2 = rad(eLat);

        double diff_lat = red_lat1 - red_lat2;
        double diff_long = rad(sLon) - rad(eLon);

        double s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(diff_lat / 2), 2) +
                Math.cos(red_lat1) * Math.cos(red_lat2) * Math.pow(Math.sin(diff_long / 2), 2)));
        s *= EARTH_RADIUS;

        return (int) s;
    }

    /**
     * 获取曼哈顿距离
     * @param sLon 起点经度
     * @param sLat 起点纬度
     * @param eLon 终点经度
     * @param eLat 终点纬度
     * @return 距离/米
     */
    public static int getManhattanDistance(double sLon, double sLat, double eLon, double eLat) {
        double offset_x = Math.abs(sLon - eLon) * Math.PI / 180 * EARTH_RADIUS / 1000;
        double offset_y = Math.abs(sLat - eLat) * Math.PI / 180 * EARTH_RADIUS / 1000;
        return (int) Math.floor((offset_x + offset_y) * 1000);
    }
}
