package com.lifeisftc.easywork.server.commons.datasource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 动态数据源
 * @author gongyunlong
 */
public class DynamicDataSource extends AbstractRoutingDataSource {
    /**
     * 获取到动态数据源
     * AbstractRoutingDataSource类中存储着一个map
     * 里边存储的是用这个方法的返回值当key，去拿对应的数据源
     * @return Object
     */
    @Override
    protected Object determineCurrentLookupKey() {
        if (DynamicDataSourceHolder.isMaster()){
            return DynamicDataSourceEnum.MASTER.getValue();
        }
        return DynamicDataSourceEnum.SLAVE.getValue();
    }
}
