package com.lifeisftc.easywork.server.commons.metrics.monitor;

import com.lifeisftc.easywork.server.commons.metrics.IMonitorObserver;
import com.lifeisftc.easywork.server.commons.metrics.base.CommonUtils;
import com.lifeisftc.easywork.server.commons.metrics.entity.DimensionStatisticsEntity;
import com.lifeisftc.easywork.server.commons.metrics.entity.InterfaceDimensionEntity;
import io.prometheus.client.Gauge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 总调用次数统计
 * @author gongyunlong
 */
public class TotalCallMonitor implements IMonitorObserver {

    private static Logger logger = LoggerFactory.getLogger(TotalCallMonitor.class);

    private static Gauge gauge = Gauge.build().name("server_total_count").help("about total count").labelNames("app", "endpoint", "method", "type").register();

    public TotalCallMonitor() {
        logger.info("TotalCallMonitor init...");
    }

    @Override
    public String name() {
        return "TotalCallMonitor";
    }

    @Override
    public void update(InterfaceDimensionEntity now, InterfaceDimensionEntity past) {
        if(now.getTotalCopy() == null) {
            return;
        }

        for(Map.Entry<String, DimensionStatisticsEntity> entry: now.getTotalCopy().entrySet()) {
            gauge.labels(CommonUtils.getAppName(), CommonUtils.getHostName(), entry.getKey().replaceAll("\\.", "_"), entry.getValue().getType()).set(entry.getValue().getCount());
        }
    }
}
