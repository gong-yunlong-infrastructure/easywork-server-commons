/**
 * Copyright (c) 2005-2012 springside.org.cn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.lifeisftc.easywork.server.commons.exception;

/**
 * 自定义业务异常类
 * @author 86137
 */
public class BusinessException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int code;
	private String message;
	
	public BusinessException(int code, String msg){
		super(msg);
		this.code = code;
		this.message = msg;
	}

	public int getCode() {
		return code;
	}

	@Override
	public String getMessage() {
		return message;
	}
	
}
