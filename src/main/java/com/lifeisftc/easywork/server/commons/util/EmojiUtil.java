package com.lifeisftc.easywork.server.commons.util;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 表情符号匹配工具类
 * @author gongyunlong
 */
public class EmojiUtil {
    /**
     * UNICODE_CASE  +  CASE_INSENSITIVE：对Unicode字符进行大小写不敏感的匹配
     * 参考：https://blog.csdn.net/wusj3/article/details/88738694
     */
    private final static Pattern EMOJI = Pattern.compile ("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]",Pattern.UNICODE_CASE|Pattern.CASE_INSENSITIVE);

    /**
     * 校验字符串中是否含有表情符号
     * @param str String
     * @return boolean true是，false否
     */
    public static boolean hasEmoji(String str) {
    	if (StringUtils.isEmpty(str)) {
    		return false;
    	}
        Matcher emojiMatcher = EMOJI.matcher(str);
        return emojiMatcher.find();
    }

}
