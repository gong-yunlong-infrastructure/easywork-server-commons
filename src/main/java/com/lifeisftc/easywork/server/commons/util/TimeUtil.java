package com.lifeisftc.easywork.server.commons.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

public class TimeUtil {

	public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

	public static final String YYYYMMDDHHMMSS1 = "yyyy/MM/dd/HH/mm/ss";

	public static final String YYYY_MM_DD = "yyyy-MM-dd";

	public static final String HH_MM = "HH:mm";

	public static final String HH_MM_SS = "HH:mm:ss";

	public static final String YYYYMMDD1 = "yyyy/MM/dd";

	public static final String MMCNDDCN = "M月d日";

	public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

	public static final DateTimeFormatter YYYY_MM_DD_HH_MM_SS_DATE_TIME = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	public static String formatDate(Date date) {

		return formatDate(date, "yyyy-MM-dd HH:mm:ss");
	}

	public static String formatDate(Date date, String formatStr) {

		DateFormat sdf = new SimpleDateFormat(formatStr);

		return sdf.format(date);
	}

	
	public static Date parseTimeStr(String timeStr)
			throws ParseException {

		return parseTimeStr(timeStr, "yyyy-MM-dd HH:mm:ss");
	}

	public static Date parseTimeStr(String timeStr, String formatStr)
			throws ParseException {

		DateFormat sdf = new SimpleDateFormat(formatStr);

		return sdf.parse(timeStr);
	}
	
	public static Date getCurTimeDate() {
		
		Calendar cal = Calendar.getInstance();
		
		return cal.getTime();
	}

	public static String getCurTimeStr() {

		Calendar cal = Calendar.getInstance();

		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		return sdf.format(cal.getTime());
	}
	
	public static Date getAnotherDate(Date date,int field,int amount) {
		Calendar cal = Calendar.getInstance();
		if(date != null){
			cal.setTime(date);
		}
		cal.add(field, amount);
		return cal.getTime();
	}
	
	public static Date getAfterDayDate(int days) {
		
		return getAnotherDate(null, Calendar.DATE, days);
	}
	
	public static Date getAfterDayDate(Date date,int days) {

		return getAnotherDate(date, Calendar.DATE, days);
	}

	public static Date getAfterDayDateZero(){
		return getAfterDayDateZero(1);
	}

	public static Date getAfterDayDateZero(int i){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.add(Calendar.DATE, i);
		return cal.getTime();
	}
	public static Date getAfterMinuteDateZero(){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.add(Calendar.MINUTE, 1);
		return cal.getTime();
	}
	
	public static Date getAfterDayMinuteZero(int i){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		cal.add(Calendar.MINUTE, i);
		return cal.getTime();
	}
	
	public static Date getAfterMinuteDate(int minutes) {
		
		return getAnotherDate(null, Calendar.MINUTE, minutes);
	}
	
	public static Date getAfterMonthDate(Date date,int months) {
		
		return getAnotherDate(date, Calendar.MONTH, months);
	}
	
	public static Date getAfterYearDate(Date date,int years) {
		
		return getAnotherDate(date, Calendar.YEAR, years);
	}

	public static String getNextNDaysStr(String timeStr, String formatStr, int days) {

		Date endTime = null;
		try {
			endTime = parseTimeStr(timeStr, formatStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Calendar cal = Calendar.getInstance();

		cal.setTime(endTime);

		cal.add(Calendar.DATE, days);

		return formatDate(cal.getTime(), formatStr);
	}
	
	public static String formateSeconds(int seconds){
		
		return seconds/3600+"时"+seconds%3600/60+"分"+seconds%3600%60+"秒";
	}
	
	public static Date getSpecificTime(Date date, int hour,
			int minute, int second, int millisecond) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minute);
		cal.set(Calendar.SECOND, second);
		cal.set(Calendar.MILLISECOND, millisecond);
		return cal.getTime();
	}
	
	/**
	 * 计算时间之间的年差
	 * 
	 * @return
	 */
	public static int getYearDiff(Date first_date,Date second_date) {
		if(first_date == null || second_date == null){
			return 0;
		}
		boolean flag = false;
		int years = -1;
		try {
			first_date = parseTimeStr(formatDate(first_date, "yyyy-MM-dd"), "yyyy-MM-dd");
			second_date = parseTimeStr(formatDate(second_date, "yyyy-MM-dd"), "yyyy-MM-dd");
			if(first_date.after(second_date)){
				Date tempDate = first_date;
				first_date = second_date;
				second_date = tempDate;
				flag = true;
			}
			
			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(first_date);
        
			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(second_date);
			
			while(!cal1.after(cal2)){
			    years++;
			    cal1.add(Calendar.YEAR, 1);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
        if(flag && years>0){
        	years = Integer.parseInt("-"+years);
        }
      return years;
	}
	
	 /**  
     * 计算两个日期之间相差的天数  
     * @param smdate 较小的时间 
     * @param bdate  较大的时间 
     * @return 相差天数 
     * @throws ParseException  
     */    
    public static int daysBetween(Date smdate,Date bdate) throws ParseException{
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
        smdate=sdf.parse(sdf.format(smdate));  
        bdate=sdf.parse(sdf.format(bdate));  
        Calendar cal = Calendar.getInstance();    
        cal.setTime(smdate);   
        long time1 = cal.getTimeInMillis();                 
        cal.setTime(bdate);    
        long time2 = cal.getTimeInMillis();         
        long between_days=(time2-time1)/(1000*3600*24);  
            
        return Integer.parseInt(String.valueOf(between_days));           
    }

	/**
	 * 获取两个日期相差的秒数
	 * @param smdate
	 * @param bdate
	 * @return
	 * @throws ParseException
	 */
    public static int secondsBetween(Date smdate,Date bdate) throws ParseException {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		smdate=sdf.parse(sdf.format(smdate));
		bdate=sdf.parse(sdf.format(bdate));
		Calendar cal = Calendar.getInstance();
		cal.setTime(smdate);
		long time1 = cal.getTimeInMillis();
		cal.setTime(bdate);
		long time2 = cal.getTimeInMillis();
		long time=(time2-time1)/1000;
		return Integer.parseInt(String.valueOf(time));
	}
	
	/**
	 * 计算某时间距当前时间的天数
	 * @param date_target
	 * @return
	 */
	public static int getDaysBetweenToday(String date_target){
		Integer days = 0;
		try {
			Date beginDate = parseTimeStr(date_target, "yyyy-MM-dd");
			days = daysBetween(beginDate, new Date());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return days;
	}
    
    /**
     * 取得当前日期所在周的最后一天
     *
     * @param date
     * @return
     */
    public static Date getLastDayOfWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK,calendar.getFirstDayOfWeek() + 6);
        return calendar.getTime();
    }

    /**
     * 获取本周周一
     * @param date
     */
    public static String getFirstDayOfThisWeek(Date date){
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        return df.format(calendar.getTime());
    }

    /**
     * 获取日期所在的周
     * @param date
     * @return
     * @throws ParseException
     */
    public static int getDayOfWeek(String date) throws ParseException{
    	Calendar calendar = Calendar.getInstance();
    	calendar.setTime(TimeUtil.parseTimeStr(date, "yyyy-MM-dd"));
    	return calendar.get(Calendar.DAY_OF_WEEK);
    }
	
	/**
	 * 获得日期的年
	 * @param date
	 */
	public static int getYear(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.YEAR);
	}

	/**
	 * 是否同一天
	 * @param date1
	 * @param date2
	 */
	public static boolean isSameDate(Date date1, Date date2) {
       Calendar cal1 = Calendar.getInstance();
       cal1.setTime(date1);
       Calendar cal2 = Calendar.getInstance();
       cal2.setTime(date2);
       return  (cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)) 
    		   && (cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH)) 
    		   && (cal1.get(Calendar.DAY_OF_MONTH) == cal2.get(Calendar.DAY_OF_MONTH)); 
	}

	/**
	 * 获取今天0时0分0秒时间
	 * @return
	 */
	public static Date getTodayStartTime(){
		return getTimeOfDate(new Date(),0,0,0);
	}

	/**
	 * 获取某一天0时0分0秒时间
	 * @return
	 */
	public static Date getTodayStartTime(Date date){
		return getTimeOfDate(date,0,0,0);
	}

	/**
	 * 获取今天23时59分59秒时间
	 * @return
	 */
	public static Date getTodayEndTime(){
		return getTimeOfDate(new Date(),23,59,59);
	}

	/**
	 * 获取某一天23时59分59秒时间
	 * @return
	 */
	public static Date getTodayEndTime(Date date){
		return getTimeOfDate(date,23,59,59);
	}

	/***
	 * 获取某个日期中的某时某分某秒
	 * @param date
	 * @param hour
	 * @param minute
	 * @param second
	 * @return
	 */
	public static Date getTimeOfDate(Date  date,int hour, int minute, int second) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY,hour);
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.SECOND, second);
		Date calendarTime = calendar.getTime();
		return calendarTime;
	}

	public static void main(String[] args) throws Exception {
//		Date currentDate = new Date();
//		// 获得今天的23点这个时间
//		Date dateOf23Pm = TimeUtil.getTimeOfDate(currentDate, 23, 0, 0);
//		// 获取明天的时间
//		Date afterDayDate = TimeUtil.getAfterDayDate(1);
//		// 获取明天早上七点的时间
//		Date dateOf7Am = TimeUtil.getTimeOfDate(afterDayDate, 7, 0, 0);
//		int betweenFromNowTo23Pm = TimeUtil.secondsBetween(dateOf23Pm,currentDate);
//		int betweenFromNowTo7Am = TimeUtil.secondsBetween(currentDate, dateOf7Am);
//		if (betweenFromNowTo23Pm > 0 && betweenFromNowTo7Am > 0) {
//			System.err.print( betweenFromNowTo7Am);
//		} else {
//			System.err.print("exits");
//		}
		getFirstDayThisTear();
		System.out.println(getAfterDayDateZero());
	}
	/**
	 * 判断时间是否在两个时间区间内 true在区间内
	 * @param startTime
	 * @param endTime
	 * @param checkTime
	 * @return
	 */
	public static boolean isValidTime(Date startTime, Date endTime, Date checkTime){
		boolean flag = false;
		if(startTime.getTime() <= checkTime.getTime() && endTime.getTime() >= checkTime.getTime()){
			flag = true;
		}
		return flag;
	}

	/**
	 * 由过去的某一时间,计算距离当前的时间
	 * */
	public static String CalculateTime(Date date){
		long nowTime=System.currentTimeMillis();  //获取当前时间的毫秒数
		String msg = null;
		Date setTime = date;  //指定时间
		long reset=setTime.getTime();   //获取指定时间的毫秒数
		long dateDiff=nowTime-reset;

		if(dateDiff<0){
			msg="输入的时间不对";
		}else{
			long dateTemp1=dateDiff/1000; //秒
			long dateTemp2=dateTemp1/60; //分钟
			long dateTemp3=dateTemp2/60; //小时
			long dateTemp4=dateTemp3/24; //天数
			long dateTemp5=dateTemp4/30; //月数
			long dateTemp6=dateTemp5/12; //年数
			if(dateTemp6>0){
				msg = dateTemp6+"年前";
			}else if(dateTemp5>0){
				msg = dateTemp5+"个月前";
			}else if(dateTemp4>0){
				msg = dateTemp4+"天前";
			}else if(dateTemp3>0){
				msg = dateTemp3+"小时前";
			}else if(dateTemp2>0){
				msg = dateTemp2+"分钟前";
			}else if(dateTemp1>0){
				msg = "刚刚";
			}
		}
		return msg;
	}

	/**
	 * 某一个月第一天和最后一天
	 * @param date
	 * @return
	 */
	public static Map<String, String> getFirstday_Lastday_Month(Date date) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		//当月第一天
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		String day_first = df.format(calendar.getTime());

		//当月最后一天
		calendar.add(Calendar.MONTH, 1);    //加一个月
		calendar.add(Calendar.DATE, -1);    //再减一天即为上个月最后一天
		String day_last = df.format(calendar.getTime());

		Map<String, String> map = new HashMap<String, String>();
		map.put("first", day_first);
		map.put("last", day_last);
		return map;
	}

	public static String formatOrderDate(Date date,Integer type){
		if(date==null){
			return "";
		}
		Calendar cal = Calendar.getInstance();
		Integer currYear = cal.get(Calendar.YEAR);
		Integer currMonth = cal.get(Calendar.MONTH);
		Integer currDay = cal.get(Calendar.DAY_OF_MONTH);
		cal.setTime(date);
		Integer year = cal.get(Calendar.YEAR);
		Integer month = cal.get(Calendar.MONTH);
		Integer day = cal.get(Calendar.DAY_OF_MONTH);
		if(type!=null && type==1){
			if(currYear==year && currMonth==month && currDay==day){
				return "今天";
			}
		}
		DateFormat sdf = null;
		if(currYear.equals(year)){
			sdf = new SimpleDateFormat("MM-dd HH:mm:ss");
		}else{
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		}
		return sdf.format(date);
	}


	/**
	 * 将连续时间字符串 替换为有间隔时间字符串
	 * 例： 20141217000000 ——>>  2014-12-17 00:00:00
	 * @param timeStringWithoutSpace 没有间隙的时间字符串
	 * @return
	 */
	public static String formatTimeWithoutSpace(String timeStringWithoutSpace){
		StringBuffer sb = new StringBuffer();
		sb.append(timeStringWithoutSpace.substring(0,4));
		sb.append("-");
		sb.append(timeStringWithoutSpace.substring(4,6));
		sb.append("-");
		sb.append(timeStringWithoutSpace.substring(6,8));
		sb.append(" ");
		sb.append(timeStringWithoutSpace.substring(8,10));
		sb.append(":");
		sb.append(timeStringWithoutSpace.substring(10,12));
		sb.append(":");
		sb.append(timeStringWithoutSpace.substring(12,14));
		return sb.toString();
	}

	/**
	 * 获取本周周天
	 */
	public static String getLastDayOfThisWeek(Date date){
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		return df.format(getAfterDayDate(calendar.getTime(),1));
	}

	/**
	 *获得日期之间的周
	 */
	public static List<Map<String,String>> getWeekOfPeriod(Date begin, Date end){
		List<Map<String,String>> result = new ArrayList<Map<String,String>>();
		Map<String,String> map;
		String beginStr = formatDate(begin,"yyyy-MM-dd");
		String endStr = formatDate(end,"yyyy-MM-dd");
		String beginSunday = getLastDayOfThisWeek(begin);
		String endMonday = getFirstDayOfThisWeek(end);
		try {
			if(parseTimeStr(endMonday, "yyyy-MM-dd").before(parseTimeStr(beginStr, "yyyy-MM-dd")) || endMonday.equals(beginStr)){//endMonday<=begin
				map = new HashMap<String, String>();
				map.put("from", beginStr);
				map.put("to", endStr);
				map.put("days", String.valueOf(daysBetween(parseTimeStr(beginStr,"yyyy-MM-dd"), parseTimeStr(endStr,"yyyy-MM-dd"))+1));
				result.add(map);
			}else{
				//开始周
				map = new HashMap<String, String>();
				map.put("from", beginStr);
				map.put("to", beginSunday);
				map.put("days", String.valueOf(daysBetween(parseTimeStr(beginStr,"yyyy-MM-dd"), parseTimeStr(beginSunday,"yyyy-MM-dd"))+1));
				result.add(map);
				//中间整周
				Date midBegin = getAfterDayDate(parseTimeStr(beginSunday,"yyyy-MM-dd"),1);
				Date midEnd = getAfterDayDate(parseTimeStr(endMonday,"yyyy-MM-dd"),-1);
				while(midBegin.before(midEnd)){
					map = new HashMap<String, String>();
					map.put("from", formatDate(midBegin,"yyyy-MM-dd"));
					map.put("to", formatDate(getAfterDayDate(midBegin, 6),"yyyy-MM-dd"));
					map.put("days", "7");
					midBegin = getAfterDayDate(midBegin, 7);
					result.add(map);
				}
				//结束周
				map = new HashMap<String, String>();
				map.put("from", endMonday);
				map.put("to", endStr);
				map.put("days", String.valueOf(daysBetween(parseTimeStr(endMonday,"yyyy-MM-dd"), parseTimeStr(endStr,"yyyy-MM-dd"))+1));
				result.add(map);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static int dayForWeek(Date pTime) {
		int dayForWeek=0;
		Calendar c = Calendar.getInstance();
		c.setTime(pTime);

		if(c.get(Calendar.DAY_OF_WEEK) == 1){
			dayForWeek = 7;
		}else{
			dayForWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
		}
		return dayForWeek;
	}

	/**
	 * 获取月份最后一天
	 * @param yearMonth yyyy-MM
	 * @return
	 */
	public static String getLastDayOfMonth(String yearMonth) {
		int year = Integer.parseInt(yearMonth.split("-")[0]);  //年
		int month = Integer.parseInt(yearMonth.split("-")[1]); //月
		Calendar cal = Calendar.getInstance();
		// 设置年份
		cal.set(Calendar.YEAR, year);
		// 设置月份
		cal.set(Calendar.MONTH, month - 1);
		// 获取某月最大天数
		int lastDay = cal.getActualMaximum(Calendar.DATE);
		// 设置日历中月份的最大天数
		cal.set(Calendar.DAY_OF_MONTH, lastDay);
		// 格式化日期
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(cal.getTime());
	}

	/**
	 * 获取某月天数
	 * @param date 日期
	 * @return
	 */
	public static int getDaysOfMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 获取某一年的头一天头一天的0点0分0秒
	 * @return 例如：2021-01-01 00:00:00
	 */
	public static String getFirstDayOfTear(Integer years){
		LocalDate now = LocalDate.now();
		now = now.plusYears(years);
		Date date = Date.from(now.with(TemporalAdjusters.firstDayOfYear()).atStartOfDay(ZoneId.systemDefault()).toInstant());
		System.out.println(formatDate(date));
		return formatDate(date);
	}
	/**
	 * 获取今年的头一天头一天的0点0分0秒
	 * @return 例如：2021-01-01 00:00:00
	 */
	public static String getFirstDayThisTear(){
		return getFirstDayOfTear(0);
	}

}
