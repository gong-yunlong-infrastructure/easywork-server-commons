package com.lifeisftc.easywork.server.commons.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 对虚拟号进行限制登录
 *
 * @author gongyunlong
 */
public class VirtualNumberUtil {

    private final static Logger logger = LoggerFactory.getLogger(VirtualNumberUtil.class);

    /**
     * 判断是否为虚拟号段
     */
    public static boolean isVirtualNumber(String phone) {
//        String regex = LoadPropertyUtil.getProperty("virtual.number");
        String regex = "^((140)(.*?)|(141)(.*?)|(142)(.*?)|(143)(.*?)|(144)(.*?)|(174)(.*?)|(1349)(.*?)|(170)(.*?)|(171)(.*?)|(162)(.*?)|(165)(.*?)|(167)(.*?)|(146)(.*?)|(148)(.*?)|(149)(.*?)|(1064)(.*?)|(163)(.*?)d$)";
        logger.info("VirtualNumberUtil.isVirtualNumber regex:{}", regex);
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }
}