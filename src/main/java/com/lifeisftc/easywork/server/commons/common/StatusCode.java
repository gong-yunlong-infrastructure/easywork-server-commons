package com.lifeisftc.easywork.server.commons.common;

public final class StatusCode {

    public static final int C_SUCCESS = 0;

    public static final int C_102 = 102;
    public static final String C_102_MSG = "参数错误";

    public static final int C_104 = 104;
    public static final String C_104_MSG = "服务器错误";

    public static final int C_105 = 105;
    public static final String C_105_MSG = "系统繁忙，请稍后重试";

    public static final int C_106 = 106;
    public static final String C_106_MSG = "哎呀，网络有点慢，稍后再试一下吧";

    public static final int C_201 = 201;
    public static final String C_201_MSG = "用户信息不存在";

    public static final int C_202 = 202;
    public static final String C_202_MSG = "订单信息不存在";

    /**
     * 参数错误
     */
    public static final String ERR_MSG_EMPTY = "%s不能为空";
    public static final String ERR_MSG_MAX_LENGTH = "%s不能超过%s";

}
