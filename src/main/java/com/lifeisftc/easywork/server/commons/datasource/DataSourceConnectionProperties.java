package com.lifeisftc.easywork.server.commons.datasource;



/**
 * 数据库连接属性
 * @author gongyunlong
 */
public class DataSourceConnectionProperties {
    /**
     * 数据库url
     */
    private String url;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
