package com.lifeisftc.easywork.server.commons.metrics.monitor;

import com.lifeisftc.easywork.server.commons.metrics.IMonitorObserver;
import com.lifeisftc.easywork.server.commons.metrics.base.CommonUtils;
import com.lifeisftc.easywork.server.commons.metrics.entity.DimensionStatisticsEntity;
import com.lifeisftc.easywork.server.commons.metrics.entity.InterfaceDimensionEntity;
import io.prometheus.client.Gauge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 慢借口数量统计
 * @author gongyunlong
 */
public class SlowMethodMonitor implements IMonitorObserver {

    private static Logger logger = LoggerFactory.getLogger(SlowMethodMonitor.class);

    private static Gauge gauge = Gauge.build().name("server_slow_count").help("about slow count").labelNames("app", "endpoint", "method", "type").register();

    public SlowMethodMonitor() {
        logger.info("SlowMethodMonitor init...");
    }

    @Override
    public String name() {
        return "SlowMethodMonitor";
    }

    @Override
    public void update(InterfaceDimensionEntity now, InterfaceDimensionEntity past) {
        if(now.getSlowCopy() == null) {
            return;
        }

        for(Map.Entry<String, DimensionStatisticsEntity> entry: now.getSlowCopy().entrySet()) {
            gauge.labels(CommonUtils.getAppName(), CommonUtils.getHostName(), entry.getKey().replaceAll("\\.", "_"), entry.getValue().getType()).set(entry.getValue().getCount());
        }
    }
}
