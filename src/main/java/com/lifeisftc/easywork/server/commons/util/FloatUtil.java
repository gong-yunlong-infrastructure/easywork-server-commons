package com.lifeisftc.easywork.server.commons.util;

import java.math.BigDecimal;

/**
 *
 * @author gongyunlong
 */
public class FloatUtil {

    public static float getFloat(Object value){
        if (value == null){
            return 0f;
        }
        if (value instanceof String){
            return Float.parseFloat((String) value);
        }else if (value instanceof BigDecimal){
            return ((BigDecimal) value).floatValue();
        }else if (value instanceof Integer){
            return ((Integer) value).floatValue();
        }else {
            return 0;
        }

    }
}
