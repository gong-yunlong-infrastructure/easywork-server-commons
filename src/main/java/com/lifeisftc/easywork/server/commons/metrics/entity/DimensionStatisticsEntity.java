package com.lifeisftc.easywork.server.commons.metrics.entity;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author gongyunlong
 */
public class DimensionStatisticsEntity {

    private AtomicLong count = new AtomicLong(0);

    private String method;
    private String type;

    private DimensionStatisticsEntity(){}

    public DimensionStatisticsEntity(String method, String type) {
        this.method = method;
        this.type = type;
    }
    public void call() {
        count.incrementAndGet();
    }
    public void incrementAndGet() {
        count.incrementAndGet();
    }

    public Long getCount() {
        return count.get();
    }

    public String getType() {
        return type;
    }
}
