package com.lifeisftc.easywork.server.commons.util;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * @author gongyunlong
 */
public class CheckObjUtil {

    private static Logger logger = LoggerFactory.getLogger(CheckObjUtil.class);

    public static String getString(Object obj){
        if(obj == null){
            return "";
        }else{
            return obj.toString();
        }
    }

    public static String getStringZero(Object obj){
        if(obj == null){
            return "0";
        }else{
            if (StringUtils.isBlank(obj.toString())){
                return "0";
            }else {
                return obj.toString();
            }
        }
    }

    public static BigDecimal checkBigDecimal(BigDecimal obj){
        if(obj == null){
            return BigDecimal.ZERO;
        }else{
            return obj;
        }
    }

    public static boolean checkIsEmpty(Object... obj){
        for(Object o : obj){
            if(isEmpty(o)){
                return true;
            }
        }
        return false;
    }

    public static boolean checkIsNotEmpty(Object... obj){
        for(Object o : obj){
            if(isNotEmpty(o)){
                return true;
            }
        }
        return false;
    }

    /**
     * 判断字符串，集合，数组，map等常见对象是否为空
     * @param obj
     * @return
     */
    public static boolean isEmpty(Object obj) {

        if (obj == null) {
            return true;
        }

        if (obj instanceof String) {
            String str = (String) obj;
            return StringUtils.isBlank(str);
        }

        if (obj instanceof Collection) {
            Collection<?> col = (Collection<?>) obj;
            return col.isEmpty();
        }

        if (obj instanceof Map) {
            Map<?, ?> map = (Map<?, ?>) obj;
            return map.isEmpty();
        }
        return false;
    }

    /**
     * 判断字符串，集合，数组，map等常见对象不为空
     * @param object
     * @return
     */
    public static boolean isNotEmpty(Object object) {
        return !isEmpty(object);
    }




    public static String getDefaultString(Object obj,String defaultsStr){
        if (obj == null){
            return defaultsStr;
        }else{
            if (StringUtils.isBlank(obj.toString())){
                return defaultsStr;
            }else {
                return obj.toString();
            }
        }
    }


    public static String getDateString(Date date){
        if (date == null){
            return "";
        }else{
            return TimeUtil.formatDate(date);
        }
    }
    public static <T> T checkIsEmptyReturnNew(T obj,Class<T> cls){
        try {
            if (isEmpty(obj)){
                obj = cls.newInstance();
            }
        }catch (Exception e){
            logger.error("CheckObjUtilcheckIsEmptyReturnNew is error,obj:{},cls:{}",obj,cls,e);
        }
        return obj;
    }


    public static Integer getIntegerFromObj(Object object){
        if(object instanceof Integer){
            return (Integer)object;
        }
        try {
            return Integer.parseInt(getString(object));
        }catch (Exception e){
            logger.error("CheckObjUtil getIntegerFromObj is error :param is not String or Integer ,obj:{},and return null",object);
        }
        return null;
    }

    public static Long getLongFromObj(Object object){
        if(object instanceof Long){
            return (Long)object;
        }
        try {
            return Long.parseLong(getString(object));
        }catch (Exception e){
            logger.error("CheckObjUtil getLongFromObj is error :param is not String or Long ,obj:{},and return null",object);
        }
        return null;
    }
}
