package com.lifeisftc.easywork.server.commons.metrics.proxy;

import com.lifeisftc.easywork.server.commons.metrics.IMonitorObserver;
import com.lifeisftc.easywork.server.commons.metrics.entity.InterfaceDimensionEntity;
import com.lifeisftc.easywork.server.commons.metrics.monitor.SlowMethodMonitor;
import com.lifeisftc.easywork.server.commons.metrics.monitor.TotalCallMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author gongyunlong
 */
public class MonitorObserverProxy {
    private static Logger logger = LoggerFactory.getLogger(MonitorObserverProxy.class);
    private static List<IMonitorObserver> obs = new ArrayList<>();

    public static void update(InterfaceDimensionEntity now, InterfaceDimensionEntity past) {
        for(IMonitorObserver observer: obs) {
            try {
                observer.update(now, past);
            } catch (Exception e) {
                logger.error("update observer error: " + observer.name(), e);
            }
        }
    }

    public static void init() {
        obs.add(new SlowMethodMonitor());
        obs.add(new TotalCallMonitor());
//        obs.add(new WarnRatioMonitor());
//        obs.add(new WaveMonitor());
    }
}
