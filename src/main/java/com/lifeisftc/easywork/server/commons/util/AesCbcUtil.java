package com.lifeisftc.easywork.server.commons.util;

import com.lifeisftc.easywork.server.commons.json.JsonMapper;
import lombok.extern.slf4j.Slf4j;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * AES CBC模式加解密实现类
 * @author gongyunlong
 */
@Slf4j
public class AesCbcUtil {

    private static final String S_KEY = "b@&$1pgU6WHXBdw4";
    private static final String S_IV = "b@&$1pgU6WHXBdw4";
    private static AesCbcUtil instance = null;

    private AesCbcUtil() {

    }

    public static AesCbcUtil getInstance() {
        if (instance == null) {
            synchronized (AesCbcUtil.class) {
                if (instance == null) {
                    instance = new AesCbcUtil();
                }
            }
        }
        return instance;
    }

    /**
     * CBC加密函数
     *
     * @param sSrc           要加密的字符串
     * @param encodingFormat 编码格式
     * @param sKey           sKey
     * @param ivParameter    iv
     * @return String
     */
    public String encrypt(String sSrc, String encodingFormat, String sKey, String ivParameter) {
        Cipher cipher;
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] raw = sKey.getBytes();
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            //使用CBC模式，需要一个向量iv，可增加加密算法的强度
            IvParameterSpec iv = new IvParameterSpec(ivParameter.getBytes());
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
            byte[] encrypted = cipher.doFinal(sSrc.getBytes(encodingFormat));
            //此处使用BASE64做转码
            return new BASE64Encoder().encode(encrypted);
        } catch (Exception e) {
            log.error("AES 加密错误，sSrc：{}，encodingFormat：{}，sKey：{}，ivParameter：{}", sSrc, encodingFormat, sKey, ivParameter);
            return null;
        }
    }

    /**
     * CBC解密函数
     * @param sSrc 要解密的字符串
     * @param encodingFormat 编码格式
     * @param sKey String
     * @param ivParameter String
     * @return String
     */
    public String decrypt(String sSrc, String encodingFormat, String sKey, String ivParameter) {
        try {
            byte[] raw = sKey.getBytes(StandardCharsets.US_ASCII);
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            IvParameterSpec iv = new IvParameterSpec(ivParameter.getBytes());
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            //先用base64解密
            byte[] encrypted1 = new BASE64Decoder().decodeBuffer(sSrc);
            byte[] original = cipher.doFinal(encrypted1);
            return new String(original, encodingFormat);
        } catch (Exception ex) {
            log.error("AES 解密错误，sSrc：{}，encodingFormat：{}，sKey：{}，ivParameter：{}", sSrc, encodingFormat, sKey, ivParameter);
            return null;
        }
    }

    public static void main(String[] args) {
        Map<String,String> paramMap = new LinkedHashMap<>(8);
        paramMap.put("phone","13701367432");
        paramMap.put("source","10018");
        paramMap.put("requestId", CommonSystemUtil.getUUID());
        String paramStr = JsonMapper.toJson(paramMap);
        // 需要加密的字串
        System.out.println("参数加密前："+ paramStr);
        long lStart = System.currentTimeMillis();
        String encryptStr = AesCbcUtil.getInstance().encrypt(paramStr, "utf-8", S_KEY, S_IV);
        System.out.println("参数加密后：" + encryptStr);
        long lUseTime = System.currentTimeMillis() - lStart;
        System.out.println("加密耗时：" + lUseTime + "毫秒");
        String decryptStr = AesCbcUtil.getInstance().decrypt(encryptStr, "utf-8", S_KEY, S_IV);
        System.out.println("参数解密后：" + decryptStr);
        long lUseTime2 = System.currentTimeMillis() - lStart;
        System.out.println("解密耗时：" + (lUseTime2-lUseTime) + "毫秒");
    }
}

