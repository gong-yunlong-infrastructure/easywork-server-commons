package com.lifeisftc.easywork.server.commons.util;


import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.metadata.BaseRowModel;
import com.alibaba.excel.metadata.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

/**
 * excel工具类
 * @author gongyunlong
 */
public class EasyExcelUtil {
    private static Logger logger = LoggerFactory.getLogger(EasyExcelUtil.class);

    private static BufferedInputStream getInput(File excelFile) throws FileNotFoundException {
        String fileName = excelFile.getName();
        InputStream inputStream = new FileInputStream(excelFile);
        BufferedInputStream bufferedInputStream=new BufferedInputStream(inputStream);
        if (!fileName.toLowerCase().endsWith(".xls") && !fileName.toLowerCase().endsWith(".xlsx")) {
            logger.error("EasyExcelUtil.getInput is io error");
            throw new RuntimeException("文件格式错误！");
        }
        return bufferedInputStream;
    }

    /**
     * 读取某个 sheet 的 Excel 内部采用回调方法.
     *
     * @param excelFile   文件
     * @param sheetNo     sheetNo sheet的序号 1开始
     * @param headLineMun headLineMun 表头的行数 0开始
     * @param clazz       setClazz Model.class
     */
    public static void readExcel(File excelFile, int sheetNo, int headLineMun, Class<? extends BaseRowModel> clazz, AnalysisEventListener listener) {
        try (InputStream input = getInput(excelFile)) {
            Sheet sheet = new Sheet(sheetNo, headLineMun, clazz);
            EasyExcelFactory.readBySax(input, sheet, listener);
        } catch (IOException e) {
            logger.error("EasyExcelUtil.getInput is io error", e);
        }
    }

    /**
     * @param excelFile   文件
     * @param clazz       实体类映射，继承 BaseRowModel 类
     * @param headLineMun headLineMun 表头的
     */
    public static void readExcel(File excelFile, Class<? extends BaseRowModel> clazz, int headLineMun, AnalysisEventListener listener) {
        try (InputStream input = getInput(excelFile)) {
            ExcelReader excelReader = EasyExcelFactory.getReader(input, listener);
            List<Sheet> sheets = excelReader.getSheets();
            for (Sheet sheet : sheets) {
                sheet.setClazz(clazz);
                sheet.setHeadLineMun(headLineMun);
                excelReader.read(sheet);
            }
        } catch (IOException e) {
            logger.error("EasyExcelUtil.getInput is io error", e);
        }
    }

    /**
     *
     * @param response
     * @param fileName
     * @param clazz
     * @param sheetName
     * @param data
     */
    public static void exportExcel(HttpServletResponse response, String fileName, Class<? extends BaseRowModel> clazz,
                                   String sheetName, List<? extends BaseRowModel> data) {
        //response.setContentType("application/vnd.ms-excel");//xls
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//xlsx
        try {
            response.setHeader("Content-Disposition", "attachment;filename=" + new String(fileName.getBytes("UTF-8"), "ISO8859-1") + ".xlsx");
            Sheet sheet = new Sheet(1, 0, clazz, sheetName, null);
            writeExcel(response.getOutputStream(), sheet, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void writeExcel(OutputStream outputStream, Sheet sheet, List<? extends BaseRowModel> data) throws IOException {
        ExcelWriter writer = EasyExcelFactory.getWriter(outputStream);
        //设置自适应宽度
        sheet.setAutoWidth(true);
        writer.write(data, sheet);
        writer.finish();
        outputStream.flush();
        outputStream.close();
    }

    /*upload file to server*/
    public static String writeExcelToServer(List<? extends BaseRowModel> data,String sheetName,String uploadPath,Class<? extends BaseRowModel> clazz) throws Exception {
        Sheet sheet = new Sheet(1, 0, clazz, sheetName, null);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ExcelWriter writer = EasyExcelFactory.getWriter(bos);
        //设置自适应宽度
        sheet.setAutoWidth(true);
        writer.write(data, sheet);
        writer.finish();
        String location = FileUtil.uploadFile(bos.toByteArray(), uploadPath,"public");
        bos.flush();
        bos.close();
        return location;
    }

    public static File writeExcelToLocal(List<? extends BaseRowModel> data, Class<? extends BaseRowModel> clazz, String fileName, String sheetName) throws Exception {
        ExcelWriter excelWriter = EasyExcelFactory.getWriter(new FileOutputStream(fileName));
        Sheet sheet = new Sheet(1, 1, clazz, sheetName, null);
        sheet.setAutoWidth(true);
        excelWriter.write(data, sheet);
        excelWriter.finish();
        return new File(fileName);
    }


}
