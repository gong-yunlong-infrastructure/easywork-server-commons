package com.lifeisftc.easywork.server.commons.aop;

import com.alibaba.fastjson.JSON;
import com.lifeisftc.easywork.server.commons.common.E2ETestManager;
import com.lifeisftc.easywork.server.commons.common.SandboxConstants;
import com.lifeisftc.easywork.server.commons.flowcontrol.RedisRateLimiter;
import io.prometheus.client.Counter;
import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Order(50)
public class LogAop {

    private RedisRateLimiter rateLimiter;

    public LogAop(RedisRateLimiter rateLimiter) {
        this.rateLimiter = rateLimiter;
    }

    public final static Counter COUNTER = Counter.build().name("hotparams_flowcontrol_counter")
            .help("Hot params flow control counter.").labelNames("uri", "ip", "cid").register();
    public final static Counter CODE_COUNTER = Counter.build().name("http_server_response_code_count")
            .help("Http server response code counter.").labelNames("uri", "code", "method").register();
    public static final String RESPONSE_CODE_HEADER = "X-Biz-Code";
    public static final String RESPONSE_CODE_UNKNOWN = "UNKNOWN";
    public static final String NULL_STRING = "null";
    public static final String URI_BEST_MATCH_PATTERN= "org.springframework.web.servlet.HandlerMapping.bestMatchingPattern";

    private AroundHandler aroundHandler = new AroundHandler();

    //rs aop
    @Around(value = "execution (* com.didapinche..*.*Controller..*(..))")
    public Object around1(JoinPoint pjp) throws Throwable {
        HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        if (SandboxConstants.E2E_TEST_KEY.equals(req.getHeader(SandboxConstants.FLOW_IDENTITY_NAME))) {
            E2ETestManager.start();
        } else {
            E2ETestManager.reset();
        }
        String resource = req.getAttribute(URI_BEST_MATCH_PATTERN) != null ?
                (String)req.getAttribute(URI_BEST_MATCH_PATTERN) : req.getRequestURI();
        String cid = req.getParameterMap().get("user_cid") == null ? null : req.getParameterMap().get("user_cid")[0];
        String ip = req.getHeader("X-Forwarded-For");
        if(ip != null && ip.contains(",")){
            ip = ip.split(",")[0];
        }
        if (rateLimiter.tryAcquire(resource, ip) && rateLimiter.tryAcquire(resource, cid)) {
            Object result = aroundHandler.around((ProceedingJoinPoint) pjp);
            if(result != null && result instanceof String){
                try {
                    String code = JSON.parseObject((String)result).get("code").toString();
                    ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                            .getResponse().setHeader(RESPONSE_CODE_HEADER, code);
                    CODE_COUNTER.labels(resource, code, req.getMethod()).inc();
                } catch (Throwable e) {
                    CODE_COUNTER.labels(resource, NULL_STRING, req.getMethod()).inc();
                }
            } else {
                CODE_COUNTER.labels(resource, RESPONSE_CODE_UNKNOWN, req.getMethod()).inc();
            }
            return result;
        } else {
            COUNTER.labels(resource, StringUtils.EMPTY, StringUtils.EMPTY).inc();
            CODE_COUNTER.labels(resource, "100403", req.getMethod()).inc();
            ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                    .getResponse().setHeader(RESPONSE_CODE_HEADER, "100403");
            return "{\"code\":100403, \"message\":\"你访问的太频繁了，请稍后再试！\"}";
        }

    }

    //ws aop
    @Around(value = "execution (* com.didapinche..*.webservice.impl..*(..))")
    public Object around2(JoinPoint pjp) throws Throwable {
        return aroundHandler.around((ProceedingJoinPoint) pjp);
    }

}