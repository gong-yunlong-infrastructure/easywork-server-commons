package com.lifeisftc.easywork.server.commons.util;

/**
 * 处理String字符串的工具类
 * @author gongyunlong
 */
public class StringUtils {
    /**
     * 判断字符串是否为空
     * @param cs CharSequence
     * @return boolean
     */
    public static boolean isEmpty(CharSequence cs) {
        return cs == null || cs.length() == 0;
    }
    /**
     * 判断字符串是否为空
     * @param cs CharSequence
     * @return boolean
     */
    public static boolean isNotEmpty(CharSequence cs) {
        return !isEmpty(cs);
    }
}
