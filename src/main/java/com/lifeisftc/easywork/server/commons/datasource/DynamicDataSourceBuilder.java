package com.lifeisftc.easywork.server.commons.datasource;

import com.alibaba.druid.pool.DruidDataSource;

import java.util.HashMap;
import java.util.Map;

/**
 * 动态数据源构建类
 * @author gongyunlong
 */
public class DynamicDataSourceBuilder {
    /**
     * 主
     */
    private DruidDataSource masterDataSource;
    /**
     * 从
     */
    private DruidDataSource slaveDataSource;


    public void setMasterDataSource(DruidDataSource masterDataSource) {
        this.masterDataSource = masterDataSource;
    }

    public void setSlaveDataSource(DruidDataSource slaveDataSource) {
        this.slaveDataSource = slaveDataSource;
    }

    /**
     * 构建方法
     * @return DynamicDataSource
     */
    public DynamicDataSource build() {
        DynamicDataSource dynamicDataSource = new DynamicDataSource();
        // 这里是设置到了AbstractRoutingDataSource中，执行到determineCurrentLookupKey会进行动态选择
        Map<Object,Object> targetDataSourceMap = new HashMap<>(2);
        targetDataSourceMap.put(DynamicDataSourceEnum.MASTER.getValue(),masterDataSource);
        targetDataSourceMap.put(DynamicDataSourceEnum.SLAVE.getValue(),slaveDataSource);
        dynamicDataSource.setTargetDataSources(targetDataSourceMap);
        // 设置默认数据源
        dynamicDataSource.setDefaultTargetDataSource(targetDataSourceMap.get(DynamicDataSourceEnum.SLAVE.getValue()));
        return dynamicDataSource;
    }
}
