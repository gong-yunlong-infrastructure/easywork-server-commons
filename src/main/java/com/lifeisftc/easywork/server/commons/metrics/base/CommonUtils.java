package com.lifeisftc.easywork.server.commons.metrics.base;

import com.lifeisftc.easywork.server.commons.util.LoadPropertyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 *
 * @author gongyunlong
 */
public class CommonUtils {
    private static Logger logger = LoggerFactory.getLogger(CommonUtils.class);

    private static String host_name = null;
    private static String app_name = null;

    public static List<String> getAllKeys(Enumeration<String> now, Enumeration<String> past) {
        List<String> keys = new ArrayList<>();

        while (now.hasMoreElements()) {
            keys.add(now.nextElement());
        }

        while (past.hasMoreElements()) {
            String key = past.nextElement();
            if(!keys.contains(key)) {
                keys.add(key);
            }
        }

        return keys;
    }

    /**
     * 获取主机名称
     * @return String
     */
    public static String getHostName() {
        if(host_name == null) {
            try {
                host_name = InetAddress.getLocalHost().getHostName();
            } catch (UnknownHostException e) {
                logger.error("unknow host", e);
            }
        }
        return host_name;
    }

    /**
     * 获取项目名称（项目中配置的spring.application.name）
     * @return String
     */
    public static String getAppName() {
        if(app_name == null) {
            app_name = LoadPropertyUtil.getProperty("spring.application.name");
        }
        return app_name;
    }
}
