package com.lifeisftc.easywork.server.commons.datasource;

import com.alibaba.druid.pool.DruidDataSource;
import com.lifeisftc.easywork.server.commons.util.StringUtils;
//import com.lifeisftc.metrics.druid.DidaDruidStatLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

/**
 * @author gongyunlong
 */
public class DruidDataSourceBuilder {

    private final Logger logger = LoggerFactory.getLogger(DruidDataSourceBuilder.class);

    public DruidDataSource build(DataSourceConnectionProperties connectionProperties, DataSourceCommonProperties commonProperties) {

        DruidDataSource datasource = new DruidDataSource();

        //connection config
        datasource.setUrl(connectionProperties.getUrl());
        datasource.setUsername(connectionProperties.getUsername());
        datasource.setPassword(connectionProperties.getPassword());

        //common config
        datasource.setDriverClassName(commonProperties.getDriverClassName());
        datasource.setInitialSize(commonProperties.getInitialSize());
        datasource.setMinIdle(commonProperties.getMinIdle());
        datasource.setMaxActive(commonProperties.getMaxActive());
        datasource.setMaxWait(commonProperties.getMaxWait());

        if (commonProperties.getDefaultAutoCommit() != null){
            datasource.setDefaultAutoCommit(commonProperties.getDefaultAutoCommit());
        }
        datasource.setRemoveAbandoned(commonProperties.isRemoveAbandoned());
        if (commonProperties.getRemoveAbandonedTimeout() > 0){
            datasource.setRemoveAbandonedTimeout(commonProperties.getRemoveAbandonedTimeout());
        }
        datasource.setTimeBetweenEvictionRunsMillis(commonProperties.getTimeBetweenEvictionRunsMillis());
        datasource.setMinEvictableIdleTimeMillis(commonProperties.getMinEvictableIdleTimeMillis());
        if (StringUtils.isNotEmpty(commonProperties.getValidationQuery())){
            datasource.setValidationQuery(commonProperties.getValidationQuery());
        }
        datasource.setTestWhileIdle(commonProperties.isTestWhileIdle());
        datasource.setTestOnBorrow(commonProperties.isTestOnBorrow());
        datasource.setTestOnReturn(commonProperties.isTestOnReturn());
        try {
            datasource.setFilters(commonProperties.getFilters());
        } catch (SQLException e) {
            logger.error("DruidDataSourceBuilder druid configuration initialization filter failed.", e);
        }
        datasource.setConnectionProperties(commonProperties.getConnectionProperties());
        datasource.setConnectionInitSqls(commonProperties.getConnectionInitSqls());

        //dida metrics
        //TODO 待处理
//        datasource.setStatLogger(new DidaDruidStatLogger());
        datasource.setTimeBetweenLogStatsMillis(60000);//1分钟

        return datasource;
    }

}
