package com.lifeisftc.easywork.server.commons.exception;


import com.lifeisftc.easywork.server.commons.json.JsonMapper;

public class ErrorBean {

    public ErrorBean() {}

    public ErrorBean(Integer errorCode, String errorMsg) {
        this.code = errorCode;
        this.message = errorMsg;
    }

    private Integer code;
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String toJson() {
        return JsonMapper.toJson(this);
    }

    @Override
    public String toString() {
        return JsonMapper.toJson(this);
    }

}
