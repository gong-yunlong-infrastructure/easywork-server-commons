package com.lifeisftc.easywork.server.commons.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.util.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.net.URLEncoder;

/**
 *
 * @author gongyunlong
 */
public class DingDingNoticeUtil {

    private static Logger logger = LoggerFactory.getLogger(DingDingNoticeUtil.class);

    public static void sendNoticeMessage(String noticeContent, String robotUrl, String atMobile, boolean isAtAll) {
        try {
            if (StringUtils.isNotBlank(robotUrl)) {
                String dingtalkRobotSendMsg = "";
                if(isAtAll) {
                    dingtalkRobotSendMsg = "{\"msgtype\":\"text\",\"text\":{\"content\":\"%s\"}," +
                            "\"at\":{\"isAtAll\":true}}";
                }else {
                    String[] mobiles = atMobile.split(",");
                    StringBuffer sb = new StringBuffer();
                    for(String mobile : mobiles) {
                        sb = sb.append("\"").append(mobile).append("\",");
                    }
                    String mobile = sb.substring(0, sb.lastIndexOf(","));
                    dingtalkRobotSendMsg = "{\"msgtype\":\"text\",\"text\":{\"content\":\"%s\"}," +
                            "\"at\":{\"atMobiles\":["+mobile+"],\"isAtAll\":false}}";
                }

                dingtalkRobotSendMsg = String.format(dingtalkRobotSendMsg, noticeContent);
                HttpClient httpclient = HttpClients.createDefault();
                HttpPost httppost = new HttpPost(robotUrl);
                httppost.addHeader("Content-Type", "application/json; charset=utf-8");
                StringEntity se = new StringEntity(dingtalkRobotSendMsg, "utf-8");
                httppost.setEntity(se);
                HttpResponse response = httpclient.execute(httppost);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    String result = EntityUtils.toString(response.getEntity(), "utf-8");
                    logger.info("发送钉钉通知成功 noticeContent:{} result:{}", noticeContent, result);
                }
            }
        } catch (Exception e) {
            logger.error("发送钉钉通知异常 noticeContent:{}", noticeContent, e);
        }
    }

    public static void sendNoticeMessageWithSign(String noticeContent, String robotUrl,String secret, String atMobile, boolean isAtAll){
        try {
            if (StringUtils.isNotBlank(robotUrl)) {
                Long timestamp = System.currentTimeMillis();
                String sign = generateSignature(timestamp,secret);
                robotUrl = String.format(robotUrl+"&timestamp=%s&sign=%s",timestamp,sign );
                String dingtalkRobotSendMsg;
                if(isAtAll) {
                    dingtalkRobotSendMsg = "{\"msgtype\":\"text\",\"text\":{\"content\":\"%s\"}," +
                            "\"at\":{\"isAtAll\":true}}";
                }else {
                    String[] mobiles = atMobile.split(",");
                    StringBuffer sb = new StringBuffer();
                    for(String mobile : mobiles) {
                        sb = sb.append("\"").append(mobile).append("\",");
                    }
                    String mobile = sb.substring(0, sb.lastIndexOf(","));
                    dingtalkRobotSendMsg = "{\"msgtype\":\"text\",\"text\":{\"content\":\"%s\"}," +
                            "\"at\":{\"atMobiles\":["+mobile+"],\"isAtAll\":false}}";
                }

                dingtalkRobotSendMsg = String.format(dingtalkRobotSendMsg, noticeContent);
                HttpClient httpclient = HttpClients.createDefault();
                HttpPost httppost = new HttpPost(robotUrl);
                httppost.addHeader("Content-Type", "application/json; charset=utf-8");
                StringEntity se = new StringEntity(dingtalkRobotSendMsg, "utf-8");
                httppost.setEntity(se);
                HttpResponse response = httpclient.execute(httppost);
                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    String result = EntityUtils.toString(response.getEntity(), "utf-8");
                    logger.info("发送钉钉通知成功 noticeContent:{} result:{}", noticeContent, result);
                }
            }
        } catch (Exception e) {
            logger.error("发送钉钉通知异常 noticeContent:{}", noticeContent, e);
        }
    }

    private static String generateSignature(Long timestamp, String secret) throws Exception {

        String stringToSign =  timestamp + "\n" + secret;
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(new SecretKeySpec(secret.getBytes("UTF-8"), "HmacSHA256"));
        byte[] signData = mac.doFinal(stringToSign.getBytes("UTF-8"));
        return URLEncoder.encode(new String(Base64.encodeBase64(signData)), "UTF-8");
    }

}
