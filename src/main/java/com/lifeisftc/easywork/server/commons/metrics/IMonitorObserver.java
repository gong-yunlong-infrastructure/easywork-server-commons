package com.lifeisftc.easywork.server.commons.metrics;

import com.lifeisftc.easywork.server.commons.metrics.entity.InterfaceDimensionEntity;

/**
 * @author gongyunlong
 */
public interface IMonitorObserver {
    String name();
    void update(InterfaceDimensionEntity now, InterfaceDimensionEntity past);
}