package com.lifeisftc.easywork.server.commons.datasource;

/**
 * 动态数据源枚举类
 *
 * @author 86137
 */
public enum DynamicDataSourceEnum {
    /**
     * 主
     */
    MASTER("master"),
    /**
     * 从
     */
    SLAVE("slave");

    private String value;

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {

        return value;
    }

    DynamicDataSourceEnum(String value) {
        this.value = value;
    }
}
