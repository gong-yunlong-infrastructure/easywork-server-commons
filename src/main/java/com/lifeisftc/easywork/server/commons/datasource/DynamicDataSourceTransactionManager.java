package com.lifeisftc.easywork.server.commons.datasource;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;


/**
 * 动态数据源事务管理类
 * @author gongyunlong
 */
public class DynamicDataSourceTransactionManager extends DataSourceTransactionManager {

    public DynamicDataSourceTransactionManager(DynamicDataSource dynamicDataSource) {
        super(dynamicDataSource);
    }

    /**
     * 在TransactionInterceptor中调用，此时
     * @param transaction Object
     * @param definition TransactionDefinition 可以理解为@Transaction注解中的属性类
     */
    @Override
    protected void doBegin(Object transaction, TransactionDefinition definition) {
        if (definition.isReadOnly()){
            DynamicDataSourceHolder.useSlave();
        }else{
            DynamicDataSourceHolder.useMaster();
        }
        super.doBegin(transaction, definition);
    }

    /**
     * 在事务结束后调用，清除主从数据库状态，避免内存泄漏
     * @param transaction Object
     */
    @Override
    protected void doCleanupAfterCompletion(Object transaction) {
        super.doCleanupAfterCompletion(transaction);
        DynamicDataSourceHolder.clear();
    }


}
