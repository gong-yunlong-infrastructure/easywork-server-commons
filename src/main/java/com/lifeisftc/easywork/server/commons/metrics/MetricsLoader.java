package com.lifeisftc.easywork.server.commons.metrics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.lang.reflect.Method;

import com.lifeisftc.metrics.jvm.JvmMetrics;

/**
 * @author gongyunlong
 */
public class MetricsLoader implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger logger = LoggerFactory.getLogger(MetricsLoader.class);

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        //添加监控
		JvmMetrics.start();
        logger.info("JvmMetrics starting....");

        try{
            Class metricsClz = Class.forName("com.didapinche.metrics.executor.ExecutorMetrics");
            Method method = metricsClz.getDeclaredMethod("start");
            method.invoke(null);

            logger.info("ExecutorMetrics starting....");
        }catch(Exception e){
        }

        try{
            Class metricsClz = Class.forName("com.didapinche.metrics.tomcat.TomcatMetrics");
            Method method = metricsClz.getDeclaredMethod("start");
            method.invoke(null);
            logger.info("TomcatMetrics starting....");
        }catch(Exception e){
        }

    }
}
