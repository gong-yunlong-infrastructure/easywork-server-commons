package com.lifeisftc.easywork.server.commons.metrics.monitor;

import com.lifeisftc.easywork.server.commons.metrics.IMonitorObserver;
import com.lifeisftc.easywork.server.commons.metrics.base.CommonUtils;
import com.lifeisftc.easywork.server.commons.metrics.entity.InterfaceDimensionEntity;
import io.prometheus.client.Gauge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;

/**
 * 波动率统计
 */
public class WaveMonitor implements IMonitorObserver {
    private static Logger logger = LoggerFactory.getLogger(WarnRatioMonitor.class);

    private static Gauge gauge = Gauge.build().name("server_call_wave").help("about call wave").labelNames("app", "endpoint", "method", "type").register();

    public WaveMonitor() {
        logger.info("WaveMonitor init...");
    }

    @Override
    public String name() {
        return "WaveMonitor";
    }

    @Override
    public void update(InterfaceDimensionEntity now, InterfaceDimensionEntity past) {
        if(now.getTotalCopy() == null || past.getTotalCopy() == null) {
            return;
        }

        List<String> keys = CommonUtils.getAllKeys(now.getTotalCopy().keys(), past.getTotalCopy().keys());
        for(String key: keys) {
            String type = "";
            BigDecimal nowCount = new BigDecimal(0);
            if(now.getTotalCopy().get(key) != null) {
                nowCount = new BigDecimal(now.getTotalCopy().get(key).getCount());
                type = now.getTotalCopy().get(key).getType();
            }

            BigDecimal pastCount = new BigDecimal(1);
            if(past.getTotalCopy().get(key) != null) {
                pastCount = new BigDecimal(past.getTotalCopy().get(key).getCount());
                type = past.getTotalCopy().get(key).getType();
            }

            if(nowCount.longValue() >= 100 || pastCount.longValue() >= 100) {
                BigDecimal waveRatio = nowCount.subtract(pastCount).divide(pastCount, 2, BigDecimal.ROUND_DOWN).multiply(new BigDecimal(100));
                gauge.labels(CommonUtils.getAppName(), CommonUtils.getHostName(), key, type).set(waveRatio.doubleValue());
            }
        }
    }
}
