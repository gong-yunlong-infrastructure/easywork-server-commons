package com.lifeisftc.easywork.server.commons.aop;

import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import com.lifeisftc.easywork.server.commons.common.StatusCode;
import com.lifeisftc.easywork.server.commons.exception.ErrorBean;
import com.lifeisftc.easywork.server.commons.json.JsonMapper;
import com.lifeisftc.easywork.server.commons.metrics.InterfaceMonitor;
import com.lifeisftc.easywork.server.commons.spring.SpringUtils;
import com.lifeisftc.easywork.server.commons.util.CommonSystemUtil;
import com.lifeisftc.easywork.server.commons.util.LoadPropertyUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.omg.CORBA.SystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public class AroundHandler {

    private static Logger logger = LoggerFactory.getLogger(AroundHandler.class);
    private static Logger slowLogger = LoggerFactory.getLogger("SlowLogger");

    private static final String DEFAULT_RATE_LIMITED = "{\"code\": 106, \"message\": \"系统繁忙，请稍后重试。\"}";
    private static final String PROPERTY_PREFIX = "aurora.res.limited.";
    private static final Map<String, Object> RATE_LIMITED_RES = Maps.newConcurrentMap();

    // 方法执行的前后调用
    public Object around(ProceedingJoinPoint pjp) throws Throwable {

//        //before
//        Class clazz = pjp.getTarget().getClass();
//        String methodName = String.format("%s.%s", clazz.getSimpleName(), pjp.getSignature().getName());
//        logger.debug("----------" + methodName + " start------------");
//
//        for (Object obj : pjp.getArgs()) {
//            logger.debug(obj != null ? obj.toString() : "");
//        }
//
//        Object[] args = pjp.getArgs();
        Object result = null;
//        long start = System.currentTimeMillis();
//
//        if(checkAppVersion(ddcinfo)) {
//            try {
//                InterfaceMonitor.getInstance().addTotal(methodName, InterfaceMonitor.TYPE_INTERFACE);
//                String resourceName = clazz.getSimpleName() + "::" + pjp.getSignature().getName();
//                if (aurora == null) {
//                    aurora = SpringUtils.getBean(Aurora.class);
//                }
//                if (aurora.take(resourceName)) {
//                    result = pjp.proceed(args);
//                } else {
//                    result = RATE_LIMITED_RES.get(resourceName);
//                    if (result == null) {
//                        result = LoadPropertyUtil.getProperty(PROPERTY_PREFIX + resourceName, DEFAULT_RATE_LIMITED);
//                        RATE_LIMITED_RES.put(resourceName, result);
//                    }
//                    InterfaceMonitor.getInstance().addWarn(methodName, "fallback");
//                }
//                if (StringUtils.isNotEmpty((String) result) && ((String) result).startsWith("{")) {
//                    Map<String, Object> map = JsonMapper.json2Map((String) result);
//                    if (map != null && Integer.parseInt(String.valueOf(map.get("code"))) != 0) {
//                        if (Integer.parseInt(String.valueOf(map.get("code"))) == 104 || Integer.parseInt(String.valueOf(map.get("code"))) == 101) {
//                            InterfaceMonitor.getInstance().addFail(methodName, InterfaceMonitor.TYPE_INTERFACE);
//                        } else {
//                            InterfaceMonitor.getInstance().addWarn(methodName, InterfaceMonitor.TYPE_INTERFACE);
//                        }
//                        InterfaceMonitor.getInstance().setDuration(methodName, InterfaceMonitor.TYPE_INTERFACE, System.currentTimeMillis() - start, InterfaceMonitor.ERROR_FAIL);
//                        logger.warn("{} illegal return. code:{} msg:{}  args:{}", methodName, map.get("code"), map.get("message"), Joiner.on(",").skipNulls().join(args));
//                    } else {
//                        InterfaceMonitor.getInstance().setDuration(methodName, InterfaceMonitor.TYPE_INTERFACE, System.currentTimeMillis() - start, InterfaceMonitor.ERROR_SUCC);
//                    }
//                }
//            } catch (IllegalArgumentException e) {
//                InterfaceMonitor.getInstance().addWarn(methodName, InterfaceMonitor.TYPE_INTERFACE);
//                logger.warn(String.format("%s illegal argument. args:%s", methodName, Joiner.on(",").skipNulls().join(args)));
//                logger.warn(String.format("code&messge:%s", e.getMessage()));
//                ErrorBean errorBean = null;
//                if (StringUtils.isNotBlank(e.getMessage()) && e.getMessage().startsWith("{")) {
//                    errorBean = JsonMapper.json2Bean(e.getMessage(), ErrorBean.class);
//                }
//                //兼容message不为json的问题
//                if (null != errorBean) {
//                    if (errorBean.getCode() != null && errorBean.getCode() == StatusCode.C_102) {
//                        errorBean.setMessage(StatusCode.C_102_MSG);//统一设置成参数错误返回给app
//                    }
//                } else {
//                    errorBean = new ErrorBean(StatusCode.C_102, StatusCode.C_102_MSG);
//                }
//
//                result = JsonMapper.toJson(errorBean);
//                InterfaceMonitor.getInstance().setDuration(methodName, InterfaceMonitor.TYPE_INTERFACE, System.currentTimeMillis() - start, InterfaceMonitor.ERROR_SUCC);
//            } catch (SystemException e) {
//                if (e.getCode() == 104 || e.getCode() == 101) {
//                    setInternalServerError();
//                    InterfaceMonitor.getInstance().addFail(methodName, InterfaceMonitor.TYPE_INTERFACE);
//                    InterfaceMonitor.getInstance().setDuration(methodName, InterfaceMonitor.TYPE_INTERFACE, System.currentTimeMillis() - start, InterfaceMonitor.ERROR_FAIL);
//                } else {
//                    InterfaceMonitor.getInstance().addWarn(methodName, InterfaceMonitor.TYPE_INTERFACE);
//                    InterfaceMonitor.getInstance().setDuration(methodName, InterfaceMonitor.TYPE_INTERFACE, System.currentTimeMillis() - start, InterfaceMonitor.ERROR_SUCC);
//                }
//                logger.warn(String.format("%s SystemException failed. args:%s", methodName, Joiner.on(",").skipNulls().join(args)), e);
//                result = JsonMapper.toJson(new ErrorBean(e.getCode(), e.getMessage()));
//            } catch (Throwable e) {
//                setInternalServerError();
//                InterfaceMonitor.getInstance().addFail(methodName, InterfaceMonitor.TYPE_INTERFACE);
//                logger.error(String.format("%s failed. args:%s", methodName, Joiner.on(",").skipNulls().join(args)), e);
//                result = new ErrorBean(StatusCode.C_104, StatusCode.C_104_MSG);
//                result = JsonMapper.toJson(result);
//                InterfaceMonitor.getInstance().setDuration(methodName, InterfaceMonitor.TYPE_INTERFACE, System.currentTimeMillis() - start, InterfaceMonitor.ERROR_FAIL);
//            }
//        } else {
//            logger.warn("-- LOW version, rejected!");
//            logger.warn("-- booking minversion: " + LoadPropertyUtil.getProperty("aop.check.minversion.booking", "1.0.0"));
//            logger.warn("-- driver minversion: " + LoadPropertyUtil.getProperty("aop.check.minversion.driver", "1.0.0"));
//
//            result = JsonMapper.toJson(new ErrorBean(107, LoadPropertyUtil.getProperty("aop.check.minversion.toast", "请升级到最新版本")));
//        }
//
//        long end = System.currentTimeMillis();
//        if (end - start >= 100) {
//            InterfaceMonitor.getInstance().addSlow(methodName, InterfaceMonitor.TYPE_INTERFACE);
//            slowLogger.info(pjp.getTarget().getClass().getSimpleName()+"."+pjp.getSignature().getName() + " (cost " + (end - start) + " ms)------------");
//        }
//
////        InterfaceMonitor.getInstance().printResultLog(methodName, InterfaceMonitor.INTERFACE_TYPE_RS, result);
//
//        logger.debug("result is {}", JsonMapper.toJson(result));
//        //after
//        logger.debug("----------" + methodName + " end------------");
        return result;
    }
}