package com.lifeisftc.easywork.server.commons.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerUtil {
	private static Logger slowLogger = LoggerFactory.getLogger("SlowLogger");

	public static void logSlow(long start, long peak, String method) {
		long end = System.currentTimeMillis();
		if (end > start + peak) {
			slowLogger.info(String.format("%s cost:%sms", method, end - start));
		}
	}

}
