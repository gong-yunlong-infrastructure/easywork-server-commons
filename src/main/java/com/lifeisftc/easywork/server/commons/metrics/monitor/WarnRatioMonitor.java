package com.lifeisftc.easywork.server.commons.metrics.monitor;

import com.lifeisftc.easywork.server.commons.metrics.IMonitorObserver;
import com.lifeisftc.easywork.server.commons.metrics.base.CommonUtils;
import com.lifeisftc.easywork.server.commons.metrics.entity.DimensionStatisticsEntity;
import com.lifeisftc.easywork.server.commons.metrics.entity.InterfaceDimensionEntity;
import io.prometheus.client.Gauge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 警告率统计
 * @author gongyunlong
 */
public class WarnRatioMonitor implements IMonitorObserver {
    private static Logger logger = LoggerFactory.getLogger(WarnRatioMonitor.class);

    private static Gauge gauge = Gauge.build().name("server_warn_ratio").help("about warn ratio").labelNames("app", "endpoint", "method", "type").register();

    public WarnRatioMonitor() {
        logger.info("WarnRatioMonitor init...");
    }

    @Override
    public String name() {
        return "WarnRatioMonitor";
    }

    @Override
    public void update(InterfaceDimensionEntity now, InterfaceDimensionEntity past) {
        if(now.getTotalCopy() == null) {
            return;
        }

        for(Map.Entry<String, DimensionStatisticsEntity> entry: now.getTotalCopy().entrySet()) {
            BigDecimal totalnum = new BigDecimal(entry.getValue().getCount());
            if(totalnum.subtract(new BigDecimal(10)).longValue() < 0) {
                //总调用次数小于10次，略过
                continue;
            }
            if(now.getWarnCopy() == null || now.getWarnCopy().get(entry.getKey()) == null) {
                continue;
            }

            BigDecimal warnnum = new BigDecimal(now.getWarnCopy().get(entry.getKey()).getCount());
            BigDecimal ratio = warnnum.divide(totalnum, 2, BigDecimal.ROUND_DOWN).multiply(new BigDecimal(100));
            gauge.labels(CommonUtils.getAppName(), CommonUtils.getHostName(), entry.getKey().replaceAll("\\.", "_"), entry.getValue().getType()).set(ratio.setScale(2).doubleValue());
        }
    }
}
