package com.lifeisftc.easywork.server.commons.datasource;

/**
 * 处理（主，从）数据源根据事务注解自动切换的问题
 * @author gongyunlong
 */
public class DynamicDataSourceHolder {
    /**
     * 私有静态threadLocal 存储当前线程需要使用哪种数据源
     * true 主
     * false 从
     */
    private static final ThreadLocal<Boolean> local = new ThreadLocal<>();

    /**
     * 对外开放是否使用主数据库的方法
     * @return Boolean
     */
    public static boolean isMaster(){
        return local.get() != null && local.get();
    }

    /**
     * 设置主从属性
     * @param isMaster boolean
     */
    public static void setStatus(boolean isMaster){
        local.set(isMaster);
    }

    /**
     * 在结束后清除事务属性，避免threadLocal内存泄漏和事务出错的情况
     */
    public static void clear(){
        local.remove();
    }

    /**
     * 使用主库
     */
    public static void useMaster() {
        setStatus(true);
    }

    /**
     * 使用从库
     */
    public static void useSlave() {
        setStatus(false);
    }
}
